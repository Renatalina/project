import { Timer } from "./pages/timer";
import { TaskList } from "./pages/tasks-list";
import { Reports } from "./pages/reports";
import { Settings } from "./pages/settings";
import { SettingsCategories } from "./components/settings-categories/settings-categories";
import { ModalTask } from "./components/modal-task/modal-task";
import { DailyTaskList } from "./components/daily-task/daily-task";
import { DayReports } from "./components/reports-diagram/day-reports";
import { MonthReports } from "./components/reports-diagram/month-reports";
import { WeekReports } from "./components/reports-diagram/week-reports";
import { DayReportsHighCharts } from "./components/reports-highcharts/day-highcharts";
import { ReportsHighCharts } from "./pages/reports/reports-highcharts";


export default class Router {
  constructor(appBuffer) {
    this.appBuffer = appBuffer;
    this.routes = {}; // page classes for routes
    this.pages = {}; // already created pages (instances)
    this.setListeners();

    this.init();
  }

  init() {
    this.addDefaultRoute("task-list", TaskList);
    this.addRoute("settings/pomodoros", Settings);
    this.addRoute("task-list", TaskList);
    this.addRoute("timer", Timer);
    this.addRoute("settings/categories", SettingsCategories);
    this.addRoute("task-list/add-task", ModalTask);
    // this.addRoute("reports/day/tasks", DayReports);
    // this.addRoute("reports/day/pomodoros", DayReports);
    // this.addRoute("reports/week/tasks", WeekReports);
    // this.addRoute("reports/week/pomodoros", WeekReports);
    // this.addRoute("reports/month/tasks", MonthReports);
    // this.addRoute("reports/month/pomodoros", MonthReports);

    this.addRoute("reports", ReportsHighCharts);

    
    this.navigate(window.location.pathname);
  }

  navigate(href, data, isPopstateEvent) {
    const newUrl = window.location.origin + "/";
    const clearHref = href.replace(/^\//, "");

    let route = this.checkRouteExists(clearHref) || this.defaultRoute;

    if (!this.pages[route]) {
      const taskPage = new this.routes[route](data);
      this.pages[route] = taskPage.init(data);
    }

    let curHref = this.checkRouteExists(clearHref) ? clearHref : "";

    const method = isPopstateEvent ? "replaceState" : "pushState";
    window.history[method](newUrl + curHref, newUrl, newUrl + curHref);
  }

  checkRouteExists(route) {
    if (!route) return false;
    return this.routes[route] && route;
  }

  setListeners() {
    window.addEventListener("click", (event) => {
      let target = event.target.closest("a");
      if (target) {
        if (target.dataset.routerIgnore) return;

        event.preventDefault();
        this.navigate(
          target.getAttribute("href"),
          target.getAttribute("data-set")
        );

        return;
      }

      target = event.target.closest("button");
      if (target && target.dataset.route) {
        event.preventDefault();
        this.navigate(target.dataset.route, target.getAttribute("data-set"));
      }
    });

    window.addEventListener("popstate", (event) => {
      event.preventDefault();
      this.navigate(window.location.pathname, null, true);
    });
  }

  addRoute(name, route) {
    this.routes[name] = route;
  }

  addDefaultRoute(route) {
    this.defaultRoute = route;
  }

  removeRoute(name) {
    delete this.routes[name];
  }
}
