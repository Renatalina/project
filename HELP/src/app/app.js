require("./pages/settings/observe");

import Header from "./components/header/header";
export const header = new Header();

import { Observer } from "./pages/settings/observe";
export const workIterationChangedObserver = new Observer();

import { ServiceDataBase } from "./service/services-data-base";
export const serviceDB = new ServiceDataBase();

import { EventBusTimer } from "./pages/timer/eventbus-timer";
export const timerEventBus=new EventBusTimer();

import { DB } from "./service/data-base";
export const db = new DB();

/* root component starts here */
require("assets/less/main.less"); // include general styles

require("./router"); // include router

/* example of including header component */
require("./components/header/header");
require("./service/services-data-base");
require("./components/modal-task/modal-task");
require("./components/settings-categories/settings-categories");

require("./pages/tasks-list/index");
require("./pages/settings/index");
require("./pages/reports/index");
require("./pages/timer/index");

import Router from "./router";
const route = new Router();
