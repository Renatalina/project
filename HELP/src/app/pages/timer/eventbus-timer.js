export class EventBusTimer {
  constructor() {
    this.eventbus = [];
    this.eventbusReturn=[];
  }

  zeroEven(){
    this.eventbus=[];
    this.eventbusReturn=[];
  }

  subscribe(handler) {    
    this.eventbus.push(handler);
  }

  subscribeReturn(handler){ 
    this.eventbusReturn.push(handler);
  }

  notify(data) {
    this.eventbus.forEach((handler) => handler(data));
  }

  notifyReturn(data){
    this.eventbusReturn.forEach((handler)=>handler(data));
  }
}
