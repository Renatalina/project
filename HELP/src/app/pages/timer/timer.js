require("./timer.less");
import { db } from "../../app";
import { header } from "../../app";

import estimation from "../../components/navbar/timer-estimation.hbs";
import { CreateTimer } from "../../components/timer/timer-canvas";
import { timerEventBus } from "../../app";
import { Timestamp } from "firebase/firestore/lite";

export class Timer {
  constructor(idTask) {
    console.log("contructor timer");

    timerEventBus.zeroEven();
    const eventUpdateEstimation = (data) => this.updateEstimation(data);
    timerEventBus.subscribe(eventUpdateEstimation);

    // this.init(idTask);
  }

  init(idTask) {
    // db.getTasks().then((responce) => {
    //   responce.filter((elem) => {
    //     if (elem.id === idTask) {
    //       this.task = elem;
    //     }
    //   });
    //  this.initTask();
    //   });

    this.main = document.getElementsByTagName("main")[0];
    this.main.innerHTML = "";
    this.header = document.getElementsByTagName("h2")[0];
    this.initSettings();
    this.pomodoroArrayEstimation = [];

    const storage = JSON.parse(localStorage.getItem("tasks"));
    for (let i = 0; i < storage.length; i++) {
      if (+storage[i].id === +idTask) {
        this.task = storage[i];
        this.initTask();
      }
    }
  }

  initTask() {
    this.mainHeader = document.getElementsByClassName("header-h2")[0];
    this.header.innerText = `${this.task.title}`;
    header.update();

    for (let i = 0; i < this.task.estimationUsed; i++) {
      this.pomodoroArrayEstimation.push({});
    }

    this.initNavbar();
    this.initFooter();
    this.createStartTimer();
  }

  initNavbar() {
    this.nav = document.getElementsByTagName("nav");
    for (let i = 0; i < this.nav.length; i++) {
      this.nav[i].innerHTML = "";
    }
    this.description = document.createElement("p");
    this.description.classList.add("white", "font-12", "col", "text-center");
    this.description.innerText = "";
    this.description.innerText = `${this.task.description}`;
    this.nav[0].classList.remove("row");
    this.nav[0].classList.add("col", "col-md-7", "col-s-10");
    this.nav[0].appendChild(this.description);
    this.initEstimation();
  }

  initFooter() {
    this.footer = document.getElementsByTagName("footer");
    for (let i = 0; i < this.footer.length; i++) {
      this.footer[i].innerHTML = "";
    }
  }

  initEstimation() {
    const div = document.createElement("div");
    div.classList.add(
      "timer-tomato",
      "center",
      "text-center",
      "row",
      "estimation-pomodorro"
    );

    div.innerHTML = estimation({
      estimat: this.pomodoroArrayEstimation,
    });
    this.nav[0].appendChild(div);
    this.divEstimation = document.getElementsByClassName(
      "estimation-pomodorro"
    )[0];
    this.addEventListenerAddPomodoro();
  }

  initSettings() {
    // db.getSettings().then(settings=>{
    // this.workiteration=+settings.workiteration;
    // this.worktime=+settings.worktime;
    // this.longbreak=+settings.longbreak;
    // this.shortbreak=+settings.shortbrea;

    // })
    this.workiteration = 2;
    this.worktime = 20;
    this.longbreak = 30;
    this.shortbreak = 5;
  }

  addEventListenerAddPomodoro() {
    this.iconAddPomodoro = document.getElementById("add-pomodoro");
    this.iconAddPomodoro.addEventListener("click", this.addPomodoro.bind(this));
  }

  addPomodoro(event) {
    const pomodor = document.createElement("div");
    pomodor.classList.add("img-tomato");

    if (this.checkAddLimitPomodoro()) {
      this.divEstimation.firstElementChild.appendChild(pomodor);
      this.pomodoroArrayEstimation.push({});
    }
  }

  checkAddLimitPomodoro() {
    if (this.divEstimation.firstElementChild.childElementCount < 10) {
      if (this.divEstimation.firstElementChild.childElementCount === 9) {
        this.iconAddPomodoro.classList.add("hidden");
      }
      return true;
    } else {
      return false;
    }
  }

  createStartTimer() {
    const timerCanvas = new CreateTimer(
      1,
      this.worktime,
      this.workiteration,
      this.shortbreak,
      this.longbreak
    );
    this.initArrowNav();
  }

  initArrowNav() {
    const backToGlobalList =
      document.getElementsByClassName("icon-arrow-left")[0];
    backToGlobalList.addEventListener(
      "click",
      this.returnStatusActive.bind(this)
    );
  }

  returnStatusActive() {
    this.task.isActive = false;
    db.updateTask(this.task);
    const storage = JSON.parse(localStorage.getItem("tasks"));
    for (let i = 0; i < storage.length; i++) {
      if (+storage[i].id === +this.task.id) {
        storage[i].isActive = false;
        break;
      }
    }
    localStorage.setItem("tasks", JSON.stringify(storage));
  }

  updateEstimation(dataEvent) {
    const index = this.checkCompletedTask();
    const divEstimation = document.getElementsByClassName(
      "estimation-pomodorro"
    )[0];

    if (index === "end") {
      divEstimation.children[0].lastElementChild.classList.remove("img-tomato");
      if (dataEvent === "failed") {
        this.pomodoroArrayEstimation[this.pomodoroArrayEstimation.length - 1] =
          { pomodor: false };
        divEstimation.children[0].lastElementChild.classList.add(
          "crisscross-tomato"
        );
      } else if (dataEvent === "success") {
        this.pomodoroArrayEstimation[this.pomodoroArrayEstimation.length - 1] =
          { pomodor: true };
        divEstimation.children[0].lastElementChild.classList.add("fill-tomato");
      }

      this.sentUpdateTaskDB();
    } else {
      divEstimation.children[0].children[index].classList.remove("img-tomato");

      if (dataEvent === "failed") {
        this.pomodoroArrayEstimation[index] = { pomodor: false };
        divEstimation.children[0].children[index].classList.add(
          "crisscross-tomato"
        );
      } else if (dataEvent === "success") {
        this.pomodoroArrayEstimation[index] = { pomodor: true };
        divEstimation.children[0].children[index].classList.add("fill-tomato");
      }
    }
  }

  checkPomodoroForTimer() {
    const divEstimation = document.getElementsByClassName(
      "estimation-pomodorro"
    )[0];
    const lengthDivPomodoro = divEstimation.children[0].childElementCount;

    for (let i = 0; i < lengthDivPomodoro - 1; i++) {
      if (
        divEstimation.children[0].children[i].classList.contains("img-tomato")
      ) {
        if (i === lengthDivPomodoro - 1) {
          timerEventBus.notifyReturn("finish-task");
        } else {
          timerEventBus.notifyReturn("continue");
        }
        return i;
      }
    }

    return "end";
  }

  checkCompletedTask() {
    const check = this.checkPomodoroForTimer();
    if (check === "end") {
      timerEventBus.notifyReturn("end");
    }
    return check;
  }

  sentUpdateTaskDB() {
    let fail = 0;
    let success = 0;

    this.pomodoroArrayEstimation.forEach((pomodor) => {
      if (pomodor.pomodor) {
        success++;
      } else {
        fail++;
      }
    });

    if (fail > success) {
      this.task.isInProgress = false;
      this.task.priority = 0;
    } else {
      this.task.isInProgress = true;
    }

    this.task.deadline = Timestamp.fromDate(new Date());
    this.task.estimationTotal = this.pomodoroArrayEstimation.length;
    this.task.failedPomodoros = fail;

    db.updateTask(this.task);
  }
}
