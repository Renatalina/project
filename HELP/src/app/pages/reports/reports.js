require("./reports.less");
import { header, timerEventBus } from "../../app";
import { DayReports } from "../../components/reports-diagram/day-reports";
import { MonthReports } from "../../components/reports-diagram/month-reports";
import { WeekReports } from "../../components/reports-diagram/week-reports";
import navbar from "../../components/reports-diagram/navigate.hbs";
import navbarFooter from "../../components/navbar/footer-navbar.hbs";
import mainReports from "../../components/reports-diagram/main-reports.hbs";
import { db } from "../../app";


export class Reports {
  constructor(params) {
    console.log("constructor reports");
    this.determine = params;

    this.init();
  }

  init() {
    this.main = document.getElementsByTagName("main")[0];
    this.main.innerHTML = "";
    this.main.classList.value = "";
    this.main.classList.add("main-report", "col", "col-s-10");
    this.h2 = document.getElementsByTagName("h2")[0];
    this.h2.innerText = "Reports";
    header.update();
    this.initMainElements();
    this.initNavBar();
    this.initFooter();
    this.initSideArrow();
  }

  initMainElements() {
    this.main.innerHTML = mainReports();
    this.day = document.getElementById("diagram-day");
    this.week = document.getElementById("diagram-week");
    this.month = document.getElementById("diagram-month");
    this.getTasks();
  }

  initNavBar() {
    this.nav = document.getElementsByTagName("nav")[0];
    this.nav.innerHTML = "";
    this.nav.classList.value = "";
    this.nav.classList.add("nav", "nav-daily", "row", "col-9");
    this.nav.innerHTML = navbar();
    this.addEventListenerNavBar();
  }

  addEventListenerNavBar() {
    this.navigateDay = document.getElementById("navigate-day");
    // this.navigateDay.addEventListener("click", this.addDay.bind(this));
    this.navigateWeek = document.getElementById("navigate-week");
    // this.navigateWeek.addEventListener("click", this.addWeek.bind(this));
    this.navigateMonth = document.getElementById("navigate-month");
    // this.navigateMonth.addEventListener("click", this.addMonth.bind(this));
  }
  addEventListenerFooterNavBar() {
    this.navigateTasks = document.getElementById("reports-tasks");
    this.navigateTasks.addEventListener(
      "click",
      this.addReportsTasks.bind(this)
    );
    this.navigatePomodoros = document.getElementById("reports-pomodoros");
    this.navigatePomodoros.addEventListener(
      "click",
      this.addReportsPomodoros.bind(this)
    );
  }

  initFooter() {
    const footer = document.getElementsByTagName("footer");
    for (let i = 0; i < footer.length; i++) {
      footer[i].innerHTML = "";
    }
    footer[0].classList.value = "";
    footer[0].classList.add("row", "nepal-t", "center");
    footer[0].innerHTML = navbarFooter({
      "first-link": `/pomodoros`,
      "first-nav": "Pomodoros",
      "second-link": `/tasks`,
      "second-nav": "Tasks",
    });
    this.addEventListenerFooterNavBar();
  }

  getTasks() {
    // db.getTasks().then((responce) => {
    //   this.filterTasks(responce);
    // });

    const storage = JSON.parse(localStorage.getItem("tasks"));
    const newFilteredTasks = [];
    for (let i = 0; i < storage.length; i++) {
      if (storage[i].isDone) {
        newFilteredTasks.push(storage[i]);
      }
    }

    this.filterTasks(newFilteredTasks);
  }

  filterTasks(tasks) {
    this.fullTasks = tasks;
    this.filterDay(tasks);
    this.filterWeek(tasks);
    this.filterMonth(tasks);
  }

  filterDay(tasks) {
    const toDay = new Date();

    this.fillDayDiagram(
      tasks.filter((task) => {
        const date = new Date(task.startDate.seconds * 1000);

        if (
          date.getDate() === toDay.getDate() &&
          date.getMonth() === toDay.getMonth() &&
          date.getFullYear() === toDay.getFullYear()&&task.isDone
        ) {
          return task;
        }
      })
    );
  }

  filterWeek(tasks) {
    const toDay = new Date().getTime();
    const limitData = toDay - 432000000;
    // here is a boundary our week  (5 days);
    this.fillWeekDiagram(
      tasks.filter((task) => {
        const date = task.startDate.seconds * 1000;
        if (date <= toDay && date >= limitData&&task.isDone) {
          return task;
        }
      })
    );
  }

  filterMonth(tasks) {
    const toDay = new Date().getTime();
    const limitData = toDay - 2592000000;

    this.fillMonthDiagram(
      tasks.filter((task) => {
        const date = task.startDate.seconds * 1000;
        if (date <= toDay && date >= limitData && task.isDone) {
          return task;
        }
      })
    );
  }

  initSideArrow() {}

  fillDayDiagram(tasks) {
    // const dayDiagram = new DayReports(tasks, this.determine);
  }

  fillWeekDiagram(tasks) {
    // const week = new WeekReports(tasks, this.determine);
  }

  fillMonthDiagram(tasks) {
    // const month = new MonthReports(tasks, this.determine);
  }

  addDay() {
    this.navigateDay.classList.add("white");
    this.navigateDay.classList.remove("nepal-t");
    this.navigateMonth.classList.add("nepal-t");
    this.navigateMonth.classList.remove("white");
    this.navigateWeek.classList.add("nepal-t");
    this.navigateWeek.classList.remove("white");

    // this.week.classList.add("hidden");
    // this.month.classList.add("hidden");
    // this.day.classList.remove("hidden");
  }

  addWeek() {
    this.navigateDay.classList.remove("white");
    this.navigateDay.classList.add("nepal-t");
    this.navigateMonth.classList.add("nepal-t");
    this.navigateMonth.classList.remove("white");
    this.navigateWeek.classList.remove("nepal-t");
    this.navigateWeek.classList.add("white");
    // this.day.classList.add("hidden");
    // this.month.classList.add("hidden");
    // this.week.classList.remove("hidden");
  }

  addMonth() {
    this.navigateDay.classList.remove("white");
    this.navigateDay.classList.add("nepal-t");
    this.navigateMonth.classList.remove("nepal-t");
    this.navigateMonth.classList.add("white");
    this.navigateWeek.classList.add("nepal-t");
    this.navigateWeek.classList.remove("white");
    // this.day.classList.add("hidden");
    // this.week.classList.add("hidden");
    // this.month.classList.remove("hidden");
  }

  addReportsPomodoros(event) {
    this.navigatePomodoros.classList.add("white");
    this.navigatePomodoros.classList.remove("nepal-t");
    this.navigateTasks.classList.add("nepal-t");
    this.navigateTasks.classList.remove("white");
    const url = window.location.pathname.match("reports/.+/")[0] + "pomodoros";
    const btn = event.target;
    btn.setAttribute("data-route", `${url}`);
  }

  addReportsTasks(event) {
    this.navigatePomodoros.classList.remove("white");
    this.navigatePomodoros.classList.add("nepal-t");
    this.navigateTasks.classList.remove("nepal-t");
    this.navigateTasks.classList.add("white");
    const url = window.location.pathname.match("reports/.+/")[0] + "tasks";
    const btn = event.target;
    btn.setAttribute("data-route", `${url}`);
  }
}
