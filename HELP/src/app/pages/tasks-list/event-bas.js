export class EventBus {
  constructor() {
    this.eventbus = [];
  }

  subscribe(handler) {
    this.eventbus.push(handler);
  }

  notify(data) {
    this.eventbus.forEach((handler) => handler(data));
  }
}
