require("./tasks-list.less");
import { header } from "../../app";
import { DailyTaskList } from "../../components/daily-task/daily-task";
import { GlobalList } from "../../components/global-list/global-list";
import { FirstEntry } from "../../components/first-entry/first-entry";
import navbarHandlebars from "../../components/navbar/navbar.hbs";
import navbarHandlebarsRemove from "../../components/navbar/navbar-remove.hbs";

import { db } from "../../app";

import { EventBus } from "./event-bas";
import { Timestamp } from "firebase/firestore/lite";
export const taskChangedEventBas = new EventBus();

export class TaskList {
  constructor(params) {
    console.log("constructor-task-list");

    // const arr=JSON.parse(localStorage.getItem("tasks"));

    // for (let i=0; i<arr.length;i++){
    // db.setTask(arr[i]);
    // }

    // db.getTasks().then(responce=>{
    // localStorage.setItem("tasks", JSON.stringify(responce));
    // });

    // db.getTasks().then((responce) => {
    //   for (let i = 0; i < responce.length; i++) {
    //     responce[i].isDone = true;
    //     responce[i].failedPomodoros = 0;
    //     if (i % 10 === 0) {
    //       responce[i].isDone = false;
    //       responce[i].failedPomodoros = 3;
    //       responce[i].estimationTotal = responce[i].estimationUsed + 3;
    //     }
    //     db.updateTask(responce[i]);
    //   }
    //   localStorage.setItem("tasks", JSON.stringify(responce));
    // });

    this.init(params);
  }

  init(params) {
    this.main = document.getElementsByTagName("main")[0];
    this.navbar = document.getElementsByTagName("nav")[0];
    this.parameterOfTasks = params;

    this.main.innerHTML = "";
    this.main.classList.value = "";
    if (this.parameterOfTasks) {
      this.main.classList.add("remove-list", "col", "col-s-10", "center");
      localStorage.setItem("daily-remove", JSON.stringify([]));
      localStorage.setItem("global-remove", JSON.stringify([]));
    } else {
      this.main.classList.add("add", "main-daily", "col", "col-s-10", "center");
      header.hideCountRemove();
    }

    this.initNavBar();
    this.initFooter();

    if (localStorage.getItem("user")) {
      this.fillDailyTask();
      this.fillGlobalList();
    } else {
      this.firstEntry = new FirstEntry();
    }
  }

  initNavBar() {
    this.title = document.getElementsByTagName("h2")[0];
    this.title.innerText = "Daily Task List";
    header.update();

    this.linkAdd = document.createElement("a");
    this.linkAdd.href = "task-list/add-task";

    this.spanAdd = document.createElement("span");
    this.spanAdd.classList.add("icon-add", "white");
    this.linkAdd.appendChild(this.spanAdd);

    this.title.appendChild(this.linkAdd);

    this.navbar.innerHTML = "";
    this.navbar.classList.value = "";
    this.navbar.classList.add("nav", "nav-daily", "nav-remove", "row", "col-9");

    this.navbar.innerHTML = navbarHandlebars({
      "first-link": "to-do",
      "first-nav": "To Do",
      "second-link": "done",
      "second-nav": "Done",
    });

    if (this.parameterOfTasks) {
      const div = document.createElement("div");
      div.classList.add(
        "div-nav",
        "row",
        "remove-select",
        "col-s-10",
        "a-center",
        "j-start"
      );
      div.innerHTML = navbarHandlebarsRemove();
      this.navbar.prepend(div);
    }
  }

  initFooter() {
    this.footer = document.getElementsByTagName("footer");
    for (let i = 0; i < this.footer.length; i++) {
      this.footer[i].innerHTML = "";
    }
    this.footer[0].innerHTML = "";
    this.footer[0].classList.value = "";
    this.footer[0].classList.add("footer-daily", "col", "col-10", "center");
  }

  fillDailyTask() {
    this.dailyTask = new DailyTaskList(this.parameterOfTasks);
  }

  fillGlobalList() {
    this.globalList = new GlobalList(this.parameterOfTasks);
  }

  followAddTask() {
    // this.modalTask = new ModalTask("Add Task");
  }
}
