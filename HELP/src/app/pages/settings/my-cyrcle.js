import { workIterationChangedObserver } from "../../app";
import footerCanvas from "./canvas.hbs";
import { db } from "../../app";

export class CanvasLifeCyrcle {
  constructor() {
    this.main = document.getElementsByClassName("main")[0];
    this.createSectionCanvas();
    this.canvas = document.getElementById("myCanvas");
    this.valueFromDb = [];

    db.getSettings()
      .then((responce) => {
        this.valueFromDb = responce;
        this.initCanvas();
      })
      .catch((error) => {
        this.valueFromDb[0] = {
          worktime: 25,
          workiteration: 5,
          shortbreak: 5,
          longbreak: 30,
        };
        this.initCanvas();
      });
  }

  initCanvas() {
    if (this.canvas) {
      this.ctx = this.canvas.getContext("2d");
      this.cycleGraphScale = document.getElementById("div-up");
      this.cycleGraphTimeLine = document.getElementById("div-hour");
      this.work = +this.valueFromDb[0].worktime;
      this.iteration = +this.valueFromDb[0].workiteration;
      this.shortBreak = +this.valueFromDb[0].shortbreak;
      this.longBreak = +this.valueFromDb[0].longbreak;

      this.drawCanvas();
      const observeUpdateCanvas = (data) => this.updateChangeTime(data);
      workIterationChangedObserver.subscribe(observeUpdateCanvas);
    }
  }

  updateChangeTime(changedElem) {
    if (changedElem.id === "settings-worktime-time") {
      this.work = +changedElem.innerText;
    } else if (changedElem.id === "settings-iterationtime-time") {
      this.iteration = +changedElem.innerText;
    } else if (changedElem.id === "settings-shorttime-time") {
      this.shortBreak = +changedElem.innerText;
    } else {
      this.longBreak = +changedElem.innerText;
    }
    this.drawCanvas();
  }

  appendParagraphIcon(nodeParent, firstElem, secondElem) {
    nodeParent.appendChild(firstElem);
    nodeParent.appendChild(secondElem);

    return nodeParent;
  }

  addDivSpotTimer(classHtml = "div-part") {
    const divPart = document.createElement("div");
    divPart.classList.add("col", classHtml);
    const i = document.createElement("i");
    i.classList.add("material-icons", "maliby-t");
    i.innerText = "brightness_1";
    const p = document.createElement("p");
    p.classList.add("hour-paragraph", "font-12");

    return { node: divPart, i, p };
  }

  fillDivUpSpotTimer(text) {
    const elem = this.addDivSpotTimer("div-up-hour");
    elem.p.innerText = text;
    this.cycleGraphScale.appendChild(
      this.appendParagraphIcon(elem.node, elem.p, elem.i)
    );
  }

  drawHourUpDiv() {
    this.cycleGraphScale.innerHTML = "";

    const hour = Math.round(this.totalMinute / 60);
    const lastMinute = this.totalMinute - hour * 60;

    const hourHalf = Math.floor(this.totalMinute / 2 / 60);
    const minuteHalf = Math.round(this.totalMinute / 2 - hourHalf * 60);

    this.fillDivUpSpotTimer("0m");
    this.fillDivUpSpotTimer(`Full cyrcle: ${hourHalf}h${minuteHalf}m`);
    this.fillDivUpSpotTimer(`${hour}h${lastMinute}m`);
  }

  drawHour() {
    this.cycleGraphTimeLine.innerHTML = "";

    const widthDivHour =
      (this.cycleGraphTimeLine.offsetWidth /
        this.totalMinute /
        this.cycleGraphTimeLine.offsetWidth) *
      100 *
      30;

    for (let i = 0, hour = 1; i < (this.totalMinute / 60) * 2 - 1; i++) {
      const div = this.addDivSpotTimer();
      if (i === 0) {
        div.p.innerText = `${30}m`;
      } else if (i % 2 === 0) {
        div.p.innerText = `${hour}h${30}m`;
        hour++;
      } else {
        div.p.innerText = `${hour}h`;
      }
      const nodeChild = this.appendParagraphIcon(div.node, div.i, div.p);
      nodeChild.style.width = `${widthDivHour}%`;
      this.cycleGraphTimeLine.appendChild(nodeChild);
    }
  }

  addLongBreak(start, long) {
    this.ctx.fillStyle = "#B470D0";
    this.ctx.fillRect(start, 0, long, 10);
  }

  drawCanvas() {
    if (this.ctx) {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

      let start = 0;
      let end = 0;

      this.totalMinute =
        (this.work * this.iteration + this.shortBreak * (this.iteration - 1)) *
          2 +
        this.longBreak;

      const lenghtWork = (this.work * this.canvas.width) / this.totalMinute;
      const long = (this.longBreak * this.canvas.width) / this.totalMinute;
      const short = (this.shortBreak * this.canvas.width) / this.totalMinute;

      this.drawHour();
      this.drawHourUpDiv();

      for (let j = 0; j < 2; j++) {
        for (let i = 0; i < this.iteration; i++) {
          this.ctx.fillStyle = "#FFB200";
          this.ctx.fillRect(start, 0, lenghtWork, 10);

          end = start + lenghtWork;

          if (i !== this.iteration - 1) {
            this.ctx.fillStyle = "#57A6DC";
            this.ctx.fillRect(end, 0, short, 10);
          }
          start = end + short;
        }
        if (j < 1) {
          this.addLongBreak(start - short, long);
          start = start - short + long;
          end = start + long;
        }
      }
    }
  }

  createSectionCanvas() {
    this.sectionCanvas = document.createElement("footer");
    this.sectionCanvas.classList.add("your-cycle", "col", "col-s-10", "center");
    this.sectionCanvas.innerHTML = footerCanvas();
    this.main.appendChild(this.sectionCanvas);
  }
}
