require("./settings.less");

import { header } from "../../app";
import { TimeWorkIterationPart } from "./time-table";
import { CanvasLifeCyrcle } from "./my-cyrcle";
import settings from "./settings.hbs";
import navbar from "../../components/navbar/navbar.hbs";
import { db } from "../../app";

export class Settings {
  constructor() {
    console.log("constructor-settings");
    // this.init();
  }

  init() {
    this.main = document.getElementsByTagName("main")[0];
    this.initHeader();
    this.valueSettingsDb = [];
    db.getSettings()
      .then((responce) => {
        this.idSettingsDB = responce[0].id;
        this.valueSettingsDb = responce;
        this.workspaceInitHandlebars();
      })
      .catch((error) => {
        this.valueSettingsDb[0] = {
          worktime: 25,
          workiteration: 5,
          shortbreak: 5,
          longbreak: 30,
        };

        this.workspaceInitHandlebars();
      });

    this.initFooter();
    if (document.getElementsByTagName("footer")[1])
      document.getElementsByTagName("footer")[1].innerHTML = "";
    const canvas = new CanvasLifeCyrcle();
    canvas.drawCanvas();
  }

  initWorkIteration() {
    this.workTime = new TimeWorkIterationPart({
      min: 15,
      max: 25,
      step: 5,
      node: "settings-worktime-time",
      minus: "settings-minus-worktime",
      plus: "settings-plus-worktime",
    });
    this.iterationTime = new TimeWorkIterationPart({
      min: 2,
      max: 5,
      step: 1,
      node: "settings-iterationtime-time",
      minus: "settings-minus-iterationtime",
      plus: "settings-plus-iterationtime",
    });
    this.shortTime = new TimeWorkIterationPart({
      min: 3,
      max: 5,
      step: 1,
      node: "settings-shorttime-time",
      minus: "settings-minus-shorttime",
      plus: "settings-plus-shorttime",
    });
    this.longTime = new TimeWorkIterationPart({
      min: 15,
      max: 30,
      step: 5,
      node: "settings-longtime-time",
      minus: "settings-minus-longtime",
      plus: "settings-plus-longtime",
    });
  }

  fillMainElem() {
    this.sectionPart = document.createElement("section");

    this.sectionPart.classList.add(
      "pomodoro-settings",
      "col",
      "col-s-8",
      "col-md-9",
      "col-lg-7",
      "center"
    );
    this.articleFirst = document.createElement("article");

    this.articleFirst.classList.add(
      "pomodoro-first-workspace",
      "col",
      "col-md-5"
    );
    this.sectionPart.appendChild(this.articleFirst);
    this.articleSecond = document.createElement("article");

    this.articleSecond.classList.add(
      "pomodoro-second-workspace",
      "col",
      "col-md-5"
    );

    this.sectionPart.appendChild(this.articleSecond);
    this.main.prepend(this.sectionPart);
  }

  initMainElem() {
    this.main.innerHTML = "";
    this.main.classList.value = "";

    this.main.classList.add(
      "main",
      "pomodoro-settings",
      "main-pomodoro-settings",
      "col",
      "col-s-9",
      "center",
      "mt-1"
    );

    this.fillMainElem();
  }

  workspaceInitHandlebars() {
    const contextArticleFirst = {
      workspace: [
        {
          name: "WORK TIME",
          space: "worktime",
          time: this.valueSettingsDb[0].worktime,
          min: 15,
          max: 25,
          iterator: "minutes",
        },
        {
          name: "WORK ITERATION",
          space: "iterationtime",
          time: this.valueSettingsDb[0].workiteration,
          min: 2,
          max: 5,
          iterator: "iteration",
        },
      ],
    };
    const contextArticleSecond = {
      workspace: [
        {
          name: "SHORT BREAK",
          space: "shorttime",
          time: this.valueSettingsDb[0].shortbreak,
          min: 3,
          max: 5,
          iterator: "minutes",
        },
        {
          name: "LONG BREAK",
          space: "longtime",
          time: this.valueSettingsDb[0].longbreak,
          min: 15,
          max: 30,
          iterator: "minutes",
        },
      ],
    };

    this.articleFirst.innerHTML = settings(contextArticleFirst);
    this.articleSecond.innerHTML = settings(contextArticleSecond);

    this.initWorkIteration();
  }

  initHeader() {
    this.initMainElem();
    this.title = document.getElementsByClassName("h2-header-daily")[0];
    this.title.innerText = "Settings";
    header.update();
    this.navbar = document.getElementsByClassName("nav")[0];
    this.navbar.classList.remove("row");
    this.navbar.classList.add("col");
    this.navbar.innerHTML = navbar({
      "navbar-title": "Pomodoros",
      "first-link": "settings/pomodoros",
      "first-nav": "Pomodoros",
      "second-link": "settings/categories",
      "second-nav": "Categories",
    });
  }

  initFooter() {
    this.footer = document.getElementsByTagName("footer")[0];
    this.footer.innerHTML = "";
    this.footer.classList.value = "";
    this.footer.classList.add(
      "footer",
      "footer-settings",
      "row",
      "col-s-9",
      "center"
    );
    this.buttonSave = document.createElement("button");
    this.buttonSave.classList.add(
      "btn",
      "btn-save",
      "col",
      "col-s-5",
      "center"
    );
    this.buttonSave.innerText = "Save";
    this.buttonSave.addEventListener("click", this.saveSettings.bind(this));
    this.buttonSave.setAttribute("data-route", "task-list");
    this.buttonGoToTask = document.createElement("button");
    this.buttonGoToTask.classList.add(
      "btn",
      "btn-go",
      "col",
      "col-s-5",
      "center"
    );
    this.buttonGoToTask.innerText = "Go To Task";
    this.buttonGoToTask.setAttribute("data-route", "task-list");
    this.footer.appendChild(this.buttonGoToTask);
    this.footer.appendChild(this.buttonSave);
  }

  saveSettings() {
    const updateSettings = {
      worktime: this.workTime.mainElem.innerText,
      workiteration: this.iterationTime.mainElem.innerText,
      shortbreak: this.shortTime.mainElem.innerText,
      longbreak: this.longTime.mainElem.innerText,
      id: this.idSettingsDB,
    };

    db.updateSettings(updateSettings)
      .then((responce) => {
        console.log("Succeful save settings ");
      })
      .catch((error) => {
        console.log(error);
      });
  }

}
