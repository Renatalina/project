import { workIterationChangedObserver } from "../../app";

export class TimeWorkIterationPart {
  constructor(objElement) {
    this.mainElem = document.getElementById(objElement.node);
    if (this.mainElem) {
      this.valueDefault = +this.mainElem.innerText;
      this.min = objElement.min;
      this.max = objElement.max;
      this.step = objElement.step;
      this.minus = document.getElementById(objElement.minus);
      this.minus.addEventListener("click", this.clickOnMinus.bind(this));
      this.plus = document.getElementById(objElement.plus);
      this.plus.addEventListener("click", this.clickOnPlus.bind(this));
    }
  }

  clickOnMinus() {
    if (this.mainElem.innerText <= this.min) {
      this.minus.classList.add("disabled");
    } else {
      this.mainElem.innerText = +this.mainElem.innerText - this.step;
      this.plus.classList.remove("disabled");
    }
    this.notifyChanges(this.mainElem);
  }

  clickOnPlus() {
    if (+this.mainElem.innerText >= this.max) {
      this.plus.classList.add("disabled");
    } else {
      this.mainElem.innerText = +this.mainElem.innerText + this.step;
      this.minus.classList.remove("disabled");
    }
    this.notifyChanges(this.mainElem);
  }

  returnToDefaultValue() {
    this.mainElem.innerText = this.valueDefault;
    workIterationChangedObserver.notify(this.mainElem);
  }

  notifyChanges(newValue) {
    workIterationChangedObserver.notify(newValue);
  }
}
