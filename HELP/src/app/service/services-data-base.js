import { DB } from "./data-base";
import { Timestamp } from "firebase/firestore/lite";

export class ServiceDataBase {
  constructor() {
    this.dataBase = new DB();
    this.taskList = [];
  }

  getTaskList() {
    this.dataBase
      .getTasks()
      .then((responce) => {
        console.log(responce);
        this.taskList = responce;
        return responce;
      })
      .catch((reject) => {
        console.error(reject);
      });
  }

  setNewTask(task) {
    this.dataBase.setTask(task);
  }

  updateExistTask(task) {
    this.dataBase.updateTask(task);
  }
}

// const task={
//     id:7,
//     title: "Lorem",
//     description: "But what we need to do in than on situation and Gracias del morro fe lide ar benore",
//     createDate: Timestamp.fromDate(new Date("December 10, 2021")),
//     startDate: Timestamp.fromDate(new Date("March 5, 2022")),
//     deadline: Timestamp.fromDate(new Date("March 15, 2022")),
//     isActive:false,
//     estimationTotal:15,
//     estimationUsed:20,
//     priority: 3,
//     categoryId: "work",
//     isInProgress:false
// };

// serviceDB.getTaskList();
// serviceDB.setNewTask(task);
// serviceDB.updateExistTask(task);
