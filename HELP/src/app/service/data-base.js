import "babel-polyfill";
import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  getDocs,
  setDoc,
  Timestamp,
  doc,
  addDoc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore/lite";

export class DB {
  constructor() {
    this.firebaseConfig = {
      apiKey: "AIzaSyD9oRV9ji-BCBgz1DUxmLfGzHZ9OGC5XIQ",
      authDomain: "zorro-pomodorro.firebaseapp.com",
      projectId: "zorro-pomodorro",
      storageBucket: "zorro-pomodorro.appspot.com",
      messagingSenderId: "655062309866",
      appId: "1:655062309866:web:7d02fda82f77f3745c7093",
    };

    this.intialize = initializeApp(this.firebaseConfig);
    this.db = getFirestore(this.intialize);
  }

  async getTasks() {
    const taskCol = collection(this.db, "task");
    const taskSnapshot = await getDocs(taskCol);
    const taskList = taskSnapshot.docs.map((doc) => doc.data());
    return taskList;
  }

  setTask(task) {
    const setTask = setDoc(doc(this.db, "task", `${task.id}`), task);
    return setTask;
  }

  updateTask(task) {
    const setTask = setDoc(doc(this.db, "task", `${task.id}`), task);
    return setTask;
  }

  deleteTask(task) {
    const delTask = deleteDoc(doc(this.db, "task", `${task.id}`), task);
  }

  async getSettings() {
    const settingsCol = collection(this.db, "settings");
    const settingsSnapshot = await getDocs(settingsCol);
    const settings = settingsSnapshot.docs.map((doc) => doc.data());
    return settings;
  }

  setSettings(tools) {
    const setSettin = setDoc(doc(this.db, "settings", `${tools.id}`), tools);
    return setSettin;
  }

  updateSettings(tools) {
    const update = updateDoc(doc(this.db, "settings", `${tools.id}`), tools);
    return update;
  }
}
