require("./reports-diagram.less");

import weekTask from "./week-month.hbs";
import { Reports } from "../../pages/reports";

export class WeekReports {
  constructor(params) {
   this.reports=new Reports(params);
    this.mainElem = document.getElementById("column-unite-week");
    this.listTasksWeek = [];
    this.fullTasks=[];
    this.demand=params; 
    this.getTasks(); 
 
  }
  init(params){  ; 
    console.log("this init from week reports");
  }
  getTasks() {
    // db.getTasks().then((responce) => {
          //  this.filterDay(responce);
    // this.fullTasks=responce;
    // });

    const storage = JSON.parse(localStorage.getItem("tasks")); 

    for(let i=0; i<storage.length;i++){
         if(storage[i].isDone){
          this.fullTasks.push(storage[i]);
         }
    }
    this.filterWeek(this.fullTasks);    
  }

  filterWeek(tasks) {
    const toDay = new Date().getTime();
    const limitData = toDay - 432000000;
    // here is a boundary our week  (5 days);
    this.listTasksWeek=
      tasks.filter((task) => {
        const date = task.startDate.seconds * 1000;
        if (date <= toDay && date >= limitData) {
          return task;
        }
      });
      this.createListWeekDiagram();
  }

  filterListByPriorityDiagram(listTasks) {
    const lists = {
      urgent: [],
      height: [],
      middle: [],
      low: [],
      failed: [],
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent.push(listTasks[i]);
          break;
        case 3:
          lists.height.push(listTasks[i]);
          break;
        case 2:
          lists.middle.push(listTasks[i]);
          break;
        case 1:
          lists.low.push(listTasks[i]);
          break;
        case 0:
          lists.failed.push(listTasks[i]);
          break;
        default:
          break;
      }
    }

    return lists;
  }

  filterListByPriorityPomodoroDiagram(listTasks) {
    const lists = {
      urgent: 0,
      height: 0,
      middle: 0,
      low: 0,
      failed: 0,
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 3:
          lists.height+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 2:
          lists.middle+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 1:
          lists.low+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 0:
          lists.failed+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        default:
          break;
      }
    }

    return lists;
  }


  filterByWeek() {
    let toDay = new Date().getTime();
    let limitData = "";
    let listWithFilterTask = [];

    for (let i = 0; i < 5; i++) {
      limitData = toDay - 86400000;

      listWithFilterTask[i] = this.listTasksWeek.filter((task) => {
        const date = task.startDate.seconds * 1000;

        if (date <= toDay && date >= limitData) {
          return task;
        }
      });

      toDay = toDay - 86400000;
    }

    return listWithFilterTask;
  }

  createTaskObjForHandlebars(fullFilteredTasks) {
    const obj = [];
    const weekDay = [
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
    ];

    for (let i = 0; i < fullFilteredTasks.length; i++) {
      obj.push(
        new Object({
          task: [
            {
              background: "urgent",
              height: `${fullFilteredTasks[i].urgent.length * 10}%`,
              countTask: fullFilteredTasks[i].urgent.length,
              determine:"tasks",      
            },
            {
              background: "high",
              height: `${fullFilteredTasks[i].height.length * 10}%`,
              countTask: fullFilteredTasks[i].height.length,
              determine:"tasks",                   
            },
            {
              background: "middle",
              height: `${fullFilteredTasks[i].middle.length * 10}%`,
              countTask: fullFilteredTasks[i].middle.length,
              determine:"tasks",     
            },
            {
              background: "low",
              height: `${fullFilteredTasks[i].low.length * 10}%`,
              countTask: fullFilteredTasks[i].low.length,
              determine:"tasks",     
            },
          ],
          failed: {
            height: `${fullFilteredTasks[i].failed.length}rem`,
            countTask: fullFilteredTasks[i].failed.length,
            determine:"tasks",     
          },
          weekDay: `${weekDay[new Date().getDay() - 1 + i]}`,
        })
      );
    }
    return obj;
  }
  createPomodoroObjForHandlebars(fullFilteredTasks) {
    const obj = [];
    const weekDay = [
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
    ];

    for (let i = 0; i < fullFilteredTasks.length; i++) {
      obj.push(
        new Object({
          task: [
            {
              background: "urgent",
              height: `${fullFilteredTasks[i].urgent * 10}%`,
              countTask: fullFilteredTasks[i].urgent,
              determine:"tasks",     
            },
            {
              background: "high",
              height: `${fullFilteredTasks[i].height * 10}%`,
              countTask: fullFilteredTasks[i].height,
              determine:"tasks",     
            },
            {
              background: "middle",
              height: `${fullFilteredTasks[i].middle * 10}%`,
              countTask: fullFilteredTasks[i].middle,
              determine:"tasks",     
            },
            {
              background: "low",
              height: `${fullFilteredTasks[i].low * 10}%`,
              countTask: fullFilteredTasks[i].low,
              determine:"tasks",     
            },
          ],
          failed: {
            height: `${fullFilteredTasks[i].failed}rem`,
            countTask: fullFilteredTasks[i].failed,
            determine:"tasks",     
          },
          weekDay: `${weekDay[new Date().getDay() - 1 + i]}`,
        })
      );
    }
    return obj;
  }

  createListWeekDiagram() {
    const filterTasks = this.filterByWeek();    

    const obj = this.initByRouters(filterTasks);
    this.createTasks(obj);
  }

  initByRouters(filterTasks){
    let fullFilteredTasks = [];
    filterTasks.forEach((list) => {  
      if(this.demand==="tasks"){
        fullFilteredTasks.push(this.filterListByPriorityDiagram(list));
      }else{
        fullFilteredTasks.push(this.filterListByPriorityPomodoroDiagram(list));
      }    
      
    });
    if(this.demand==="tasks"){
     return this.createTaskObjForHandlebars(fullFilteredTasks);
    }else {
      return this.createPomodoroObjForHandlebars(fullFilteredTasks);
    }
    
    
  }

  createTasks(obj) {
    this.mainElem.innerHTML = weekTask({ day: obj });
  }
}
