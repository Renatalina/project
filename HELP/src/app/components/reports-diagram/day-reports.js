require("./reports-diagram.less");
import dayReports from "./day-reports.hbs";
import { Reports } from "../../pages/reports";
import { Timestamp } from "firebase/firestore/lite";

export class DayReports {
  constructor(params) {
    this.reports = new Reports(params);
    this.mainElemDiagram = document.getElementById("column-unite-day");
    this.fullTasks = [];
    this.listDayTasks = [];

    this.demand = params;
    this.getTasks();
  }

  init(params) {
    console.log("this init day");
  }

  getTasks() {
    // db.getTasks().then((responce) => {
    //   this.filterDay(responce);
    //   this.fullTasks = responce;
    // });

    const storage = JSON.parse(localStorage.getItem("tasks"));

    for (let i = 0; i < storage.length; i++) {
      if (storage[i].isDone) {
        this.fullTasks.push(storage[i]);
      }
    }
    this.filterDayTasks(this.fullTasks);
  }

  filterDayTasks(tasks) {
    const toDay = new Date();

    this.listDayTasks = tasks.filter((task) => {
      const date = new Date(task.startDate.seconds * 1000);
      if (
        date.getDate() === toDay.getDate() &&
        date.getMonth() === toDay.getMonth() &&
        date.getFullYear() === toDay.getFullYear()
      ) {
        return task;
      }
    });

    if (this.demand === "tasks") {
      this.createListDayDiagram();
    } else {
      this.createListDayPomodoroDiagram();
    }
  }

  filterListByPriorityDiagram(listTasks) {
    const lists = {
      urgent: [],
      height: [],
      middle: [],
      low: [],
      failed: [],
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent.push(listTasks[i]);
          break;
        case 3:
          lists.height.push(listTasks[i]);
          break;
        case 2:
          lists.middle.push(listTasks[i]);
          break;
        case 1:
          lists.low.push(listTasks[i]);
          break;
        case 0:
          lists.failed.push(listTasks[i]);
          break;
        default:
          break;
      }
    }

    return lists;
  }

  createListDayDiagram() {
    const filterTasks = this.filterListByPriorityDiagram(this.listDayTasks);

    const obj = [
      {
        height: `${filterTasks.urgent.length * 10}%`,
        text: "URGENT",
        background: "tomato",
        determine: "tasks",
        countTasks: filterTasks.urgent.length,
      },
      {
        height: `${filterTasks.height.length * 10}%`,
        text: "HIGHT",
        background: "carrot",
        determine: "tasks",
        countTasks: filterTasks.height.length,
      },
      {
        height: `${filterTasks.middle.length * 10}%`,
        text: "MIDDLE",
        background: "gorse",
        determine: "tasks",
        countTasks: filterTasks.middle.length,
      },
      {
        height: `${filterTasks.low.length * 10}%`,
        text: "LOW",
        background: "turquoise",
        determine: "tasks",
        countTasks: filterTasks.low.length,
      },
      {
        height: `${filterTasks.failed.length * 10}%`,
        text: "FAILED",
        background: "nepal",
        determine: "tasks",
        countTasks: filterTasks.failed.length,
      },
    ];

    this.addTask(obj);
  }

  filterListByPomodoroDiagram(listTasks) {
    const lists = {
      urgent: 0,
      height: 0,
      middle: 0,
      low: 0,
      failed: 0,
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 3:
          lists.height +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 2:
          lists.middle +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 1:
          lists.low +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 0:
          lists.failed +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        default:
          break;
      }
    }

    return lists;
  }
  createListDayPomodoroDiagram() {
    const filterTasks = this.filterListByPomodoroDiagram(this.listDayTasks);
    const obj = [
      {
        height: `${filterTasks.urgent * 10}%`,
        text: "URGENT",
        background: "tomato",
        determine: "pomodoros",
        countTasks: filterTasks.urgent,
      },
      {
        height: `${filterTasks.height * 10}%`,
        text: "HIGHT",
        background: "carrot",
        determine: "pomodoros",
        countTasks: filterTasks.height,
      },
      {
        height: `${filterTasks.middle * 10}%`,
        text: "MIDDLE",
        background: "gorse",
        determine: "pomodoros",
        countTasks: filterTasks.middle,
      },
      {
        height: `${filterTasks.low * 10}%`,
        text: "LOW",
        background: "turquoise",
        determine: "pomodoros",
        countTasks: filterTasks.low,
      },
      {
        height: `${filterTasks.failed * 10}%`,
        text: "FAILED",
        background: "nepal",
        determine: "pomodoros",
        countTasks: filterTasks.failed,
      },
    ];

    this.addTask(obj);
  }

  addTask(obj) {
    this.mainElemDiagram.innerHTML = dayReports({ day: obj });
  }
}
