require("./reports-diagram.less");

import weekMonth from "./week-month.hbs";
import { Reports } from "../../pages/reports";

export class MonthReports {
  constructor(params) {
    console.log("month reports");
    this.reports=new Reports(params);
    this.mainElem = document.getElementById("column-unite-month");
    this.demand=params; 
    this.fullTasks=[];
    this.listTasksWeek = [];
    this.getTasks();
       
  }
  init(params){    
    console.log("init month");
  }
  getTasks() {
    // db.getTasks().then((responce) => {
          //  this.filterDay(responce);
    // this.fullTasks=responce;
    // });

    const storage = JSON.parse(localStorage.getItem("tasks")); 

    for(let i=0; i<storage.length;i++){
         if(storage[i].isDone){
          this.fullTasks.push(storage[i]);
         }
    }
    this.filterMonth(this.fullTasks);    
  }


  filterMonth(tasks) {
    const toDay = new Date().getTime();
    const limitData = toDay - 2592000000;

    this.listTasksWeek=
      tasks.filter((task) => {
        const date = task.startDate.seconds * 1000;
        if (date <= toDay && date >= limitData) {
          return task;
        }
      });  
    this.createListWeekDiagram();
  }


  filterListByPriorityDiagram(listTasks) {
    const lists = {
      urgent: [],
      height: [],
      middle: [],
      low: [],
      failed: [],
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent.push(listTasks[i]);
          break;
        case 3:
          lists.height.push(listTasks[i]);
          break;
        case 2:
          lists.middle.push(listTasks[i]);
          break;
        case 1:
          lists.low.push(listTasks[i]);
          break;
        case 0:
          lists.failed.push(listTasks[i]);
          break;
        default:
          break;
      }
    }

    return lists;
  }
  filterListByPriorityPomodoroDiagram(listTasks) {
    const lists = {
      urgent: 0,
      height: 0,
      middle: 0,
      low: 0,
      failed: 0,
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 3:
          lists.height+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 2:
          lists.middle+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 1:
          lists.low+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        case 0:
          lists.failed+=listTasks[i].estimationTotal-listTasks[i].failedPomodoros;
          break;
        default:
          break;
      }
    }

    return lists;
  }

  filterByWeek() {
    let toDay = new Date().getTime();
    this.day = [];
    this.day.push(new Date(toDay).getDate());
    let limitData = "";
    let listWithFilterTask = [];

    for (let i = 0; i < 30; i++) {
      limitData = toDay - 86400000;

      const taskFilteredByDay = this.listTasksWeek.filter((task) => {
        const date = task.startDate.seconds * 1000;

        if (date <= toDay && date >= limitData) {
          return task;
        }
      });

      listWithFilterTask.push(taskFilteredByDay);

      toDay = toDay - 86400000;
      this.day.push(new Date(toDay).getDate());
    }
    return listWithFilterTask;
  }

  createMonthDay() {
    const getDay = new Set();
    let newListDays = [];

    for (let i = 0; i < this.listTasksWeek.length; i++) {
      getDay.add(
        new Date(this.listTasksWeek[i].startDate.seconds * 1000).getDate()
      );
    }
    getDay.forEach((value, valueAgain, getDay) => {
      newListDays.push(value);
    });
    for (let k = 0; k < newListDays.length - 1; k++) {
      let day = newListDays[k];
      for (let i = k; i < newListDays.length; i++) {
        if (newListDays[i] < newListDays[k]) {
          day = newListDays[i];
          newListDays[i] = newListDays[k];
          newListDays[k] = day;
        }
      }
    }

    return newListDays;
  }

  createFullMonthCalendar() {
    const arrDayForHandlebars = [];
    const day = new Date();
  }

  createTaskObjForHandlebars(fullFilteredTasks) {
    const obj = [];

    for (let i = 30, k = 0; i >= 0; i--, k++) {
      if (fullFilteredTasks[i]) {
        obj.push(
          new Object({
            task: [
              {
                background: "urgent",
                height: `${fullFilteredTasks[i].urgent.length * 10}%`,
                countTask: fullFilteredTasks[i].urgent.length,
                determine:"tasks",
              },
              {
                background: "high",
                height: `${fullFilteredTasks[i].height.length * 10}%`,
                countTask: fullFilteredTasks[i].height.length,
                determine:"tasks",
              },
              {
                background: "middle",
                height: `${fullFilteredTasks[i].middle.length * 10}%`,
                countTask: fullFilteredTasks[i].middle.length,
                determine:"tasks",
              },
              {
                background: "low",
                height: `${fullFilteredTasks[i].low.length * 10}%`,
                countTask: fullFilteredTasks[i].low.length,
                determine:"tasks",
              },
              {
                background: "failed",
                height: `${fullFilteredTasks[i].failed.length * 10}%`,
                countTask: fullFilteredTasks[i].failed.length,
                determine:"tasks",
              },
            ],
            weekDay: `${this.day[i]}`,
          })
        );
      }
    }
    return obj;
  }
  createPomodoroObjForHandlebars(fullFilteredTasks) {
    const obj = [];

    for (let i = 30, k = 0; i >= 0; i--, k++) {
      if (fullFilteredTasks[i]) {
        obj.push(
          new Object({
            task: [
              {
                background: "urgent",
                height: `${fullFilteredTasks[i].urgent * 10}%`,
                countTask: fullFilteredTasks[i].urgent,
                determine:"pomodoros",
              },
              {
                background: "high",
                height: `${fullFilteredTasks[i].height* 10}%`,
                countTask: fullFilteredTasks[i].height,
                determine:"pomodoros",
              },
              {
                background: "middle",
                height: `${fullFilteredTasks[i].middle * 10}%`,
                countTask: fullFilteredTasks[i].middle,
                determine:"pomodoros",
              },
              {
                background: "low",
                height: `${fullFilteredTasks[i].low * 10}%`,
                countTask: fullFilteredTasks[i].low,
                determine:"pomodoros",
              },
              {
                background: "failed",
                height: `${fullFilteredTasks[i].failed * 10}%`,
                countTask: fullFilteredTasks[i].failed,
                determine:"pomodoros",
              },
            ],
            weekDay: `${this.day[i]}`,
          })
        );
      }
    }
    return obj;
  }

  createListWeekDiagram() {
    const filterTasks = this.filterByWeek();
    const obj = this.initByRouters(filterTasks);

    this.fillDiagram(obj);
  }

  initByRouters(filterTasks){
    let fullFilteredTasks = [];
    filterTasks.forEach((list) => {  
      if(this.demand==="tasks"){
        fullFilteredTasks.push(this.filterListByPriorityDiagram(list));
      }else{
        fullFilteredTasks.push(this.filterListByPriorityPomodoroDiagram(list));
      }    
      
    });
    if(this.demand==="tasks"){
     return this.createTaskObjForHandlebars(fullFilteredTasks);
    }else {
      return this.createPomodoroObjForHandlebars(fullFilteredTasks);
    }
    
  }

  fillDiagram(obj) {
    this.mainElem.innerHTML = weekMonth({ day: obj });
  }
}
