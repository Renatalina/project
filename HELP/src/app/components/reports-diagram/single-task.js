require("./reports-diagram.less");

export class SingleTask {
  constructor() {
    this.elemDivTask = document.createElement("div");
    this.elemDivTask.classList.add("column");
    this.elemDivTask.classList.add("column-level");
    this.elemSpan = document.createElement("span");
    this.elemSpan.classList.add("tooltiptext");
    this.elemDivTask.appendChild(this.elemSpan);
  }
}
