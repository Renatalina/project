import mainHandlebars from "./main.hbs";
import footerHandlebars from "./footer.hbs";

import { timerEventBus } from "../../app";

import { db } from "../../app";
import { header } from "../../app";
import estimation from "../../components/navbar/timer-estimation.hbs";

export class CreateTimer {
  constructor(timer, worktime, workiteration, shortBreak, longBreak) {
    this.workiteration = workiteration;
    this.worktime = worktime;
    this.shortBreak = shortBreak;
    this.longBreak = longBreak;
    this.iterated = 0;
    this.end = false;
    this.init(timer, worktime);
    this.initFooter();
    this.initCanvas();

    const eventUpdateEstimation = (data) => this.stopWorking(data);
    timerEventBus.subscribeReturn(eventUpdateEstimation);
  }

  init(timer, timeTask) {
    this.main = document.getElementsByTagName("main")[0];
    this.main.innerHTML = "";
    this.main.innerHTML = mainHandlebars();

    this.time = 1;
    this.timeTask = this.worktime;
    this.step = (Math.PI * 2) / this.timeTask;
  }

  initFooter() {
    this.footer = document.getElementsByTagName("footer")[0];
    this.footer.innerHTML = "";
    this.footer.innerHTML = footerHandlebars();
    this.addEventListenerBTN();
  }

  initCanvas() {
    this.canvas = document.getElementById("canvas-timer");
    this.ctx = this.canvas.getContext("2d");
    this.text = this.canvas.getContext("2d");

    this.startTimer = 4.6985214465464;
    const math = 6.283185307717;
    this.createTextCanvasLetsDoIt();
  }

  startCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.text.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.beginPath();
    this.ctx.strokeStyle = "#8DA5B8";
    this.ctx.lineWidth = 30;
    this.endTimer += this.step;
    this.ctx.arc(150, 150, 85, this.startTimer, this.endTimer);
    this.ctx.stroke();
  }

  createTextCanvasLetsDoIt() {
    this.text.font = "1.5rem Roboto Sans MS";
    this.text.fillStyle = "white";
    this.text.textAlign = "center";
    this.text.fillText(
      "Let's do it!",
      this.canvas.width / 2,
      this.canvas.height / 2
    );
  }

  createTextCanvasCompletedTask() {
    this.text.font = "1.5rem Roboto Sans MS";
    this.text.fillStyle = "white";
    this.text.textAlign = "center";
    this.text.fillText(
      "You",
      this.canvas.width / 2,
      this.canvas.height / 2 - 20
    );
    this.text.fillText(
      "Completed",
      this.canvas.width / 2,
      this.canvas.height / 2 + 10
    );
    this.text.fillText(
      "Task",
      this.canvas.width / 2,
      this.canvas.height / 2 + 40
    );
  }
  createTextCanvasBreakIsOver() {
    this.text.font = "1.5rem Roboto Sans MS";
    this.text.fillStyle = "white";
    this.text.textAlign = "center";
    this.text.fillText(
      "Break is",
      this.canvas.width / 2,
      this.canvas.height / 2 - 10
    );
    this.text.fillText(
      "over",
      this.canvas.width / 2,
      this.canvas.height / 2 + 10
    );
  }

  createTextCanvas() {
    this.text.font = "3rem Roboto Sans MS";
    this.text.fillStyle = "white";
    this.text.textAlign = "center";
    this.text.fillText(
      `${this.time}`,
      this.canvas.width / 2,
      this.canvas.height / 2
    );
    this.text.font = "1.2rem Roboto Sans MS";
    this.text.fillText(
      `minute`,
      this.canvas.width / 2,
      this.canvas.height / 2 + 25
    );
  }

  createTextBreakCanvas() {
    this.text.font = "3rem Roboto Sans MS";
    this.text.fillStyle = "white";
    this.text.textAlign = "center";
    this.text.font = "1.5rem Roboto Sans MS";
    this.text.fillText(
      `BREAK`,
      this.canvas.width / 2,
      this.canvas.height / 2 - 25
    );
    this.text.fillText(
      `${this.time}`,
      this.canvas.width / 2,
      this.canvas.height / 2
    );
    this.text.font = "1.5rem Roboto Sans MS";
    this.text.fillText(
      `min`,
      this.canvas.width / 2,
      this.canvas.height / 2 + 25
    );
  }

  breakIsOver() {
    this.text.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.startCanvas();
    this.createTextCanvasBreakIsOver();
    this.footerBreakIsOver();
  }

  completeTask() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.text.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.endTimer = 12.283185307717;
    this.startCanvas();
    this.createTextCanvasCompletedTask();
    this.footer.innerHTML = "";
  }

  addEventListenerBTN() {
    this.startBtn = document.getElementById("start");
    this.startBtn.addEventListener("click", this.start.bind(this));

    this.failBtn = document.getElementById("fail");
    this.failBtn.addEventListener("click", this.fail.bind(this));

    this.finishBtn = document.getElementById("finish");
    this.finishBtn.addEventListener("click", this.finish.bind(this));

    this.pomodorStartBtn = document.getElementById("start-pomodor");
    this.pomodorStartBtn.addEventListener(
      "click",
      this.startContinuePomodor.bind(this)
    );

    this.finishTaskBtn = document.getElementById("finish-pomodor");
    this.finishTaskBtn.addEventListener(
      "click",
      this.finishPomodoroTask.bind(this)
    );
  }

  footerStart() {
    this.startBtn.classList.add("hidden");
    this.finishTaskBtn.classList.add("hidden");
    this.pomodorStartBtn.classList.add("hidden");
    this.failBtn.classList.remove("hidden");
    this.finishBtn.classList.remove("hidden");
  }

  footerStartPomodor() {
    this.startBtn.classList.add("hidden");
    this.failBtn.classList.add("hidden");
    this.finishBtn.classList.add("hidden");
    this.pomodorStartBtn.classList.remove("hidden");
  }

  footerBreakIsOver() {
    this.failBtn.classList.add("hidden");
    this.finishBtn.classList.add("hidden");
    this.finishTaskBtn.classList.remove("hidden");
    this.pomodorStartBtn.classList.remove("hidden");
  }

  checkIteration() {
    if (this.iterated === this.workiteration) {
      this.startLongBreakTime();
      this.iterated = 0;
    } else {
      this.startBreakTime();
    }
  }

  calcutaionCanvas(initialTime) {
    this.time = 1;
    this.step = (Math.PI * 2) / initialTime / 120;
    this.midstStep = 0;
    this.endTimer = this.startTimer + this.step;
  }

  start(result) {
    this.footerStart();
    this.calcutaionCanvas(this.worktime);
    ++this.iterated;

    this.breakInterval = setInterval(() => {
      
      if (this.time === this.timeTask + 1) {
        clearInterval(this.breakInterval);

        if (result === "finish-task") {
          this.notifyChanges("end");
          this.completeTask();
         
        } else {
          this.notifyChanges("success");
        }
      } else {
        this.startCanvas();
        this.createTextCanvas();
        this.midstStep += 1;
        if (this.midstStep === 120) {
          this.time += 1;
          this.midstStep = 0;
        }
      }
    }, 1);
  }

  startBreakTime() {
    this.footerStartPomodor();
    this.calcutaionCanvas(this.shortBreak);

    this.shortBreakInterval = setInterval(() => {
      if (this.time === this.shortBreak + 1) {
        clearInterval(this.shortBreakInterval);
        this.breakIsOver();
      } else {
        this.startCanvas();
        this.createTextBreakCanvas();
        this.midstStep += 1;
        if (this.midstStep === 120) {
          this.time += 1;
          this.midstStep = 0;
        }
      }
    }, 10);
  }

  startLongBreakTime() {
    this.footerStartPomodor();
    this.calcutaionCanvas(this.longBreak);

    this.longBreakInterval = setInterval(() => {
      if (this.time === this.longBreak + 1) {
        clearInterval(this.longBreakInterval);
        this.breakIsOver();
      } else {
        this.startCanvas();
        this.createTextBreakCanvas();
        this.midstStep += 1;
        if (this.midstStep === 120) {
          this.time += 1;
          this.midstStep = 0;
        }
      }
    }, 10);
  }

  fail() {
    clearInterval(this.breakInterval);    
    this.notifyChanges("failed");
  }

  finish() {
    clearInterval(this.breakInterval);
    this.notifyChanges("success");
  }

  startContinuePomodor() {
    clearInterval(this.shortBreakInterval);
    clearInterval(this.longBreakInterval);
    this.breakIsOver();
    this.start("success");
  }

  finishPomodoroTask() {    
    this.stopWorking("end");
    this.notifyChanges("end");
  }

  stopWorking(data) {
    if (data === "end") {
      this.text.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.completeTask();
    } else if (data === "finish-task") {
      this.footer.innerHTML = "";
      this.start(data);
    } else if (data === "continue") {
      this.checkIteration();
    }
  }

  notifyChanges(newValue) {
    timerEventBus.notify(newValue);
  }
}
