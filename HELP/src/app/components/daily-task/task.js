require("./daily-task.less");

export class TaskForDaily {
  constructor(colorDepartment, colorPomodoro, colorTitleTask) {
    this.templateElemntDailyTask = document.getElementsByClassName(
      "template-daily-task"
    )[0];
    if (this.templateElemntDailyTask) {
      const cloneTask = this.templateElemntDailyTask.content.cloneNode(true);
      this.task = cloneTask.childNodes[1];

      const newTask = cloneTask.childNodes[1].childNodes[5];

      this.shrink = cloneTask.childNodes[1].childNodes[1];
      this.shrink.classList.add(colorDepartment);

      this.iconTrash = cloneTask.childNodes[1].childNodes[1].childNodes[1];

      this.blockDate = cloneTask.childNodes[1].childNodes[3];

      this.day = cloneTask.childNodes[1].childNodes[3].childNodes[1];
      this.month = cloneTask.childNodes[1].childNodes[3].childNodes[3];

      this.title = newTask.childNodes[1].childNodes[1];
      this.title.classList.add(colorTitleTask);

      this.edit = newTask.childNodes[3].childNodes[3];
      this.remove = newTask.childNodes[3].childNodes[5];
      this.up = newTask.childNodes[3].childNodes[1];

      this.pomodoro = cloneTask.childNodes[1].childNodes[7];
      this.pomodoro.classList.add(colorPomodoro);
    }
  }

  deliteTask(colorHoverChange) {
    this.iconTrash.classList.remove("hidden");
    this.shrink.classList.add(colorHoverChange);
    this.shrink.classList.add("remove-list");
    this.blockDate.remove();
  }
}
