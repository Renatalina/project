import taskHandlebars from "./task-handlebars.hbs";
import taskDone from "./task-done.hbs";
import taskRemove from "./task-remove.hbs";
import handlebarsMove from "./task-move.hbs";
import handlebarsExcellent from "./task-excellent.hbs";
import { db } from "../../app";
import { taskChangedEventBas } from "../../pages/tasks-list";
import { Timestamp } from "firebase/firestore/lite";
import { ModalTask } from "../modal-task/modal-task";
import {header} from "../../app";
require("./daily-task.less");

export class DailyTaskList {
  constructor(params) {
    this.init(params);
    const eventUpdateTaskList = (data) => this.initUpdateTasks(data);
    taskChangedEventBas.subscribe(eventUpdateTaskList);
    
  }

  init(params) {
    
    this.listTasks = [];
    this.listDone = [];
    this.listDelite = [];
    this.parametersOfTasks = params;

    this.main = document.getElementsByTagName("main")[0];
    this.footer = document.getElementsByTagName("footer")[0];
    this.footer.innerHTML = "";

    this.sectionElementDailyTask = document.createElement("section");
    this.sectionElementDailyTask.classList.add(
      "section-daily",
      "col",
      "col-s-10",
      "col-md-9",
      "center",
      "mt-1"
    );
    this.main.appendChild(this.sectionElementDailyTask);
    this.initUpdateTasks();
    this.initNavbar();
  }

  initUpdateTasks() {

    // db.getTasks().then((response) => {
    //   localStorage.setItem("tasks", JSON.stringify(response));
    //   this.addDailyTask(response);
    // });

    const storage = JSON.parse(localStorage.getItem("tasks"));
    this.listTasks = storage;
    this.addDailyTask(storage);
  }

  initNavbar() {
    const toDo = document.getElementsByClassName("to-do")[0];
    toDo.addEventListener("click", this.showToDo.bind(this));
    const done = document.getElementsByClassName("done")[0];
    done.addEventListener("click", this.showDone.bind(this));

    if (this.parametersOfTasks==="remove") {
      const selected = document.getElementsByClassName("select-all")[0];
      selected.addEventListener("click", this.selectAll.bind(this));
      const deselect = document.getElementsByClassName("deselect-all")[0];
      deselect.addEventListener("click", this.deselectAll.bind(this));

      this.addEventListenerOfRemoveTasks();
    }
  }

  showToDo(event) {
    // db.getTasks().then((response) => {
    //   this.addDailyTask(response);
    // });

    const storage = JSON.parse(localStorage.getItem("tasks"));
    this.addDailyTask(storage);
  }

  showDone(event) {
    this.sectionElementDailyTask.innerHTML = "";
    this.main.innerHTML = "";
    setTimeout(() => {
      this.main.innerHTML = "";
      this.sectionElementDailyTask.innerHTML = "";
      this.sectionElementDailyTask.innerHTML = taskDone({
        task: this.listDone,
      });
      this.main.appendChild(this.sectionElementDailyTask);
    }, 50);
  }

  checkFindTask(taskId) {
    const storage = JSON.parse(localStorage.getItem("daily-remove"));
    for (let i = 0; i < storage.length; i++) {
      if (+storage[i] === +taskId) {
        if(i===0){
          return "0";
        }else{
          return i;
        }        
      }
    }
  }

  addRemoveTask(taskId) {    
    if(!this.checkFindTask(taskId)){
      const storage = JSON.parse(localStorage.getItem("daily-remove"));
      storage.push(taskId);
      localStorage.setItem("daily-remove", JSON.stringify(storage));
    }
  }

  deleteRemoveTask(taskId){
    const storage=JSON.parse(localStorage.getItem("daily-remove"));
    delete storage[+this.checkFindTask(taskId)];
    localStorage.setItem("daily-remove", JSON.stringify(storage));   
  }

  removeListTask(event) {
    const elem = event.target;  
    if (elem.classList.contains("icon-trash")) {      
      elem.classList.remove("icon-trash");
      elem.classList.add("icon-close");
      this.addRemoveTask(elem.getAttribute("data-set"));
    }else if(elem.classList.contains("icon-close")){
      elem.classList.add("icon-trash");
      elem.classList.remove("icon-close");
     this.deleteRemoveTask(elem.getAttribute("data-set"));
    }
    header.toCountOfRemove();
  }

  selectAll() {
    const listElemRemove = document.getElementsByClassName("daily-remove");
    const arrRemove = [];
    for (let i = 0; i < listElemRemove.length; i++) {
      listElemRemove[i].classList.remove("icon-trash");
      listElemRemove[i].classList.add("icon-close");
      arrRemove.push(listElemRemove[i].getAttribute("data-set"));
    }

    localStorage.setItem("daily-remove", JSON.stringify(arrRemove));
    header.toCountOfRemove();
  }

  deselectAll() {
    const listElemRemove = document.getElementsByClassName("daily-remove");   
    for (let i = 0; i < listElemRemove.length; i++) {
      listElemRemove[i].classList.add("icon-trash");
      listElemRemove[i].classList.remove("icon-close");      
    }
    localStorage.setItem("daily-remove", JSON.stringify([]));
    header.toCountOfRemove();
  }

  checkTaskForToDay(taskList) {
    this.listTasks = [];
    this.listDone = [];
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    const toDayDate = new Date();
  
    taskList.forEach((task) => {
      
      const date = new Date(task.startDate.seconds * 1000);
      task.day = date.getDate();
      task.month = monthNames[date.getMonth()];
      if (
        toDayDate.getFullYear() === date.getFullYear() &&
        toDayDate.getMonth() === date.getMonth() &&
        toDayDate.getDate() === date.getDate()
      ) {
        this.listTasks.push(task);
      }
      if (task.isDone) {
        this.listDone.push(task);
      }
      if (task.isDelite) {
        this.listDelite.push(task);
      }
    });
  }

  addDailyTask(taskList) {
    this.checkTaskForToDay(taskList);

    if (this.listTasks.length >= 1) {
      if (this.parametersOfTasks) {
        this.sectionElementDailyTask.innerHTML = taskRemove({
          task: this.listTasks,
        });
      } else {
        this.sectionElementDailyTask.innerHTML = taskHandlebars({
          task: this.listTasks,
        });
      }
    } else if (taskList.length > 0 && this.listTasks.length < 1) {
      this.sectionElementDailyTask.innerHTML = handlebarsExcellent();
    }
    // else if(taskList.length<1) {
    //   this.sectionElementDailyTask.innerHTML = handlebarsMove();
    // }
    //TODO: correct logic to add massage

    this.addEventListenerEdit();
    this.addEventListenerRemove();
    this.addEventListenerGoToTimer();
  }

  addEventListenerEdit() {
    const tasks = document.getElementsByClassName("icon-edit");
    for (let i = 0; i < tasks.length; i++) {
      tasks[i].addEventListener("click", this.editTask.bind(this));
    }
  }

  addEventListenerRemove() {
    const tasks = document.getElementsByClassName("trash-daily");
    for (let i = 0; i < tasks.length; i++) {
      tasks[i].addEventListener("click", this.removeTask.bind(this));
    }
  }

  addEventListenerOfRemoveTasks() {
    const tasks = document.getElementsByClassName("task-daily-remove");
    for (let i = 0; i < tasks.length; i++) {
      tasks[i].addEventListener("click", this.removeListTask.bind(this));
    }
  }

  addEventListenerGoToTimer() {
    const tasks = document.getElementsByClassName("change-pomodoro");
    for (let i = 0; i < tasks.length; i++) {
      tasks[i].addEventListener("click", this.goToTimer.bind(this));
    }
  }

  deliteTask() {}

  editTask(event) {

    const idElem = event.target.getAttribute("data-set");
    this.listTasks.filter((task) => {
      if (+task.id === +idElem) {
        delete task.day;
        delete task.month;
        const modalEditTask = new ModalTask("Edit Task");
        modalEditTask.fillModalForEdit(task);
      }
    });

    this.initUpdateTasks();
  }

  removeTask(event) {
    const idElem = event.target.getAttribute("data-set");
    this.listTasks.filter((task) => {
      if (+task.id === +idElem) {
        db.deleteTask(task);
        this.deleteTaskFromLocalStorage(task);
      }
    });

    this.initUpdateTasks();
  }

  deleteTaskFromLocalStorage(task) {
    const storage = JSON.parse(localStorage.getItem("tasks"));
    const updateStorage = storage.filter((elem) => {
      if (+elem.id !== +task.id) {
        return elem;
      }
    });

    localStorage.setItem("tasks", JSON.stringify(updateStorage));    
  }

  goToTimer(event) {    
    const elemId=event.target.getAttribute("data-set");
    const tasks=JSON.parse(localStorage.getItem("tasks"));
    for (let i=0; i<tasks.length;i++){
      if(+tasks[i].id===+elemId){
        tasks[i].isActive=true;
        db.updateTask(tasks[i]);
        break;
      }
    }
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }

  createTimer() {}

  addTitleMoveTask() {}

  addExcellentTask() {}
}
