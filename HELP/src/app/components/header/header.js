require("./header.less"); // example of including component's styles
import { db } from "../../app";

let header;

export default class Header {
  constructor() {
    if (header) {
      header.update();

      return;
    }
    this.init();
  }

  init() {
    this.createHeader();

    this.update();

    window.addEventListener("scroll", this.scrollHeader.bind(this));

    header = this;
  }

  update() {
    const title = document.getElementsByClassName("h2-header-daily")[0];
    if (title) {
      const titleText = title.innerText;

      if (titleText === "Daily Task List") {
        this.remove.classList.remove("hidden");
      } else {
        this.remove.classList.add("hidden");
      }
    }
  }

  scrollHeader() {
    if (window.scrollY > 50 && this.title) {
      this.divLogo.classList.remove("hidden");
      this.title.classList.add("hidden");
      this.add.classList.remove("hidden");
      this.header.classList.add("shadow");
    }
    if (window.scrollY < 100 && this.title) {
      this.divLogo.classList.add("hidden");
      this.title.classList.remove("hidden");
      this.add.classList.add("hidden");
      this.header.classList.remove("shadow");
    }
  }

  createHeader() {
    this.title = document.getElementsByClassName("header-h2")[0];
    if (this.title) {
      this.title.classList.add("col");
      this.title.classList.remove("row");
      this.add = this.createElement("a", "icon-add", "nepal-t", "hidden");
      this.add.href = "task-list/add-task";
      this.add.addEventListener("click", this.addNewTask.bind(this));
      this.remove = this.createElement(
        "a",
        "icon-trash",
        "nepal-t",
        "hidden",
        "relative"
      );
      this.remove.href = "task-list";
      this.remove.setAttribute("data-set", "remove");
      this.remove.setAttribute("data-active", "false");

      this.countRemove = document.createElement("button");
      this.countRemove.classList.add("header-count-remove", "hidden");
      this.countRemove.addEventListener("click", this.deleteTasks.bind(this));
      this.countRemove.setAttribute("data-set", "/task-list");
      this.remove.appendChild(this.countRemove);
      this.remove.addEventListener("click", this.removeEvent.bind(this));

      this.list = this.createElement("a", "icon-list", "white");
      this.list.href = "task-list";
      this.list.addEventListener("click", this.listEvent.bind(this));

      this.reports = this.createElement("a", "icon-statistics", "nepal-t");
      this.reports.href = "reports";
      this.reports.addEventListener("click", this.reportsEvent.bind(this));

      this.settings = this.createElement("a", "icon-settings", "nepal-t");
      this.settings.href = "settings/pomodoros";
      this.settings.addEventListener("click", this.settingsEvent.bind(this));

      this.divMenu = this.createElement(
        "div",
        "div-menu",
        "row",
        "col-10",
        "col-md-9",
        "a-center",
        "j-end"
      );
      this.img = this.createLogo("img", "logo", "/images/Logo.svg", "logo");
      this.divLogo = this.createElement(
        "div",
        "hidden",
        "header-logo",
        "col",
        "col-10",
        "a-start",
        "j-start"
      );
      this.header = this.createElement(
        "header",
        "header",
        "header-daily",
        "col",
        "a-center",
        "col-10"
      );

      this.add.appendChild(this.addToolTip("Add"));
      this.remove.appendChild(this.addToolTip("Remove"));
      this.list.appendChild(this.addToolTip("List"));
      this.reports.appendChild(this.addToolTip("Reports"));
      this.settings.appendChild(this.addToolTip("Settings"));
      this.divMenu.appendChild(this.add);
      this.divMenu.appendChild(this.remove);
      this.divMenu.appendChild(this.list);
      this.divMenu.appendChild(this.reports);
      this.divMenu.appendChild(this.settings);
      this.divLogo.appendChild(this.img);
      this.divMenu.prepend(this.divLogo);
      this.header.appendChild(this.divMenu);
      document.body.prepend(this.header);
    }
  }

  toCountOfRemove() {
    this.countRemove.classList.remove("hidden");
    const removeGlobal = JSON.parse(localStorage.getItem("global-remove"));
    const removeDaily = JSON.parse(localStorage.getItem("daily-remove"));
    this.countRemove.innerText = `${+removeDaily.length + removeGlobal.length}`;
  }

  hideCountRemove() {
    this.countRemove.classList.add("hidden");
  }

  deleteTasks() {
    const removeGlobal = JSON.parse(localStorage.getItem("global-remove"));
    const removeDaily = JSON.parse(localStorage.getItem("daily-remove"));
    const listTasks = JSON.parse(localStorage.getItem("tasks"));
    const newListTask = [];
    for (let i = 0; i < removeDaily.length; i++) {
      for (let k = 0; k < listTasks.length; k++) {
        if (+removeDaily[i] === +listTasks[k].id) {
          db.deleteTask(listTasks[k]);
          delete listTasks[k];
        }
      }
    }
    for (let i = 0; i < removeGlobal.length; i++) {
      for (let k = 0; k < listTasks.length; k++) {
        if (+removeGlobal[i] === +listTasks[k].id) {
          db.deleteTask(listTasks[k]);
          delete listTasks[k];
        }
      }
    }

    for (let i = 0; i < listTasks.length; i++) {
      if (listTasks[i]) {
        newListTask.push(listTasks[i]);
      }
    }

    localStorage.setItem("tasks", JSON.stringify(newListTask));
    this.countRemove.classList.add("hidden");
  }

  addNewTask() {
    for (let i = 0; i < this.divMenu.childElementCount; i++) {
      this.divMenu.children[i].classList.add("nepal-t");
      this.divMenu.children[i].classList.remove("white");
    }
    this.add.classList.remove("nepal-t");
    this.add.classList.add("white");
    this.remove.setAttribute("data-active", "false");
  }

  removeEvent() {
    for (let i = 0; i < this.divMenu.childElementCount; i++) {
      this.divMenu.children[i].classList.add("nepal-t");
      this.divMenu.children[i].classList.remove("white");
    }
    this.remove.classList.remove("nepal-t");
    this.remove.classList.add("white");
    this.remove.setAttribute("data-active", "true");
  }

  listEvent() {
    for (let i = 0; i < this.divMenu.childElementCount; i++) {
      this.divMenu.children[i].classList.add("nepal-t");
      this.divMenu.children[i].classList.remove("white");
    }
    this.list.classList.remove("nepal-t");
    this.list.classList.add("white");
    this.remove.setAttribute("data-active", "false");
  }

  reportsEvent() {
    for (let i = 0; i < this.divMenu.childElementCount; i++) {
      this.divMenu.children[i].classList.add("nepal-t");
      this.divMenu.children[i].classList.remove("white");
    }
    this.reports.classList.remove("nepal-t");
    this.reports.classList.add("white");
    this.remove.setAttribute("data-active", "false");
  }

  settingsEvent() {
    for (let i = 0; i < this.divMenu.childElementCount; i++) {
      this.divMenu.children[i].classList.add("nepal-t");
      this.divMenu.children[i].classList.remove("white");
    }
    this.settings.classList.remove("nepal-t");
    this.settings.classList.add("white");
    this.remove.setAttribute("data-active", "false");
  }

  addToolTip(text) {
    const toolTip = this.createElement("span", "tooltiptext", "font-14");
    toolTip.innerText = text;

    return toolTip;
  }

  createLogo(tagName, classHTML, src, alt) {
    const elem = document.createElement(tagName);
    elem.classList.add(classHTML);
    elem.src = src;
    elem.alt = alt;

    return elem;
  }

  createElement(tagName, ...classHtml) {
    const elem = document.createElement(tagName);
    classHtml.forEach((html) => {
      elem.classList.add(html);
    });

    return elem;
  }
}
