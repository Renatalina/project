require("./modal-task.less");

export class Calendar {
  constructor(params) {
    this.initCalendar();    
  }

  arrMont = {
    1: "January",
    2: "Febrary",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "Oktober",
    11: "November",
    12: "December",
  };

  initCalendar() {
    this.mainElemCalendar = document.getElementById("calendar");
    this.calendarDateForm = document.getElementById("calendar-paragraph");
    this.calendarDate = document.getElementById("calendar-date");
    this.minusMonth = document.getElementById("minus-month");
    this.month = document.getElementById("calendar-month");
    this.plusMonth = document.getElementById("plus-month");
    this.minusDay = document.getElementById("minus-day");
    this.day = document.getElementById("calendar-day");
    this.plusDay = document.getElementById("plus-day");
    this.minusYear = document.getElementById("minus-year");
    this.year = document.getElementById("calendar-year");
    this.plusYear = document.getElementById("plus-year");
    this.btnCancel = document.getElementById("calendar-cancel");
    this.btnSave = document.getElementById("calendar-save");
   

    this.addEventOnCalendar();
    this.setDate(new Date());
    this.initDay();
    
  }

  initDay() {
    const toDay = new Date();
    if (toDay.getMonth() + 1 < 10) {
      this.month.innerText = `0${toDay.getMonth() + 1}`;
    } else {
      this.month.innerText = `${toDay.getMonth() + 1}`;
    }
    if (toDay.getDate() < 10) {
      this.day.innerText = `0${toDay.getDate()}`;
    } else {
      this.day.innerText = `${toDay.getDate()}`;
    }
    this.year.innerText = `${toDay.getFullYear()}`;

    this.calendarDate.innerText = `${this.arrMont[+this.month.innerText]} ${
      this.day.innerText
    }, ${this.year.innerText}`;
  }

  addEventOnCalendar() {
    
      this.minusMonth.addEventListener("click", this.monthMinus.bind(this));
      this.plusMonth.addEventListener("click", this.monthPlus.bind(this));
      this.minusDay.addEventListener("click", this.dayMinus.bind(this));
      this.plusDay.addEventListener("click", this.dayPlus.bind(this));
      this.minusYear.addEventListener("click", this.yearMinus.bind(this));
      this.plusYear.addEventListener("click", this.yearPlus.bind(this));
      this.btnCancel.addEventListener("click", this.cancel.bind(this));
      this.btnSave.addEventListener("click", this.save.bind(this));
    
  }

  checkFullDate(checkDay, checkMonth, checkYear) {
    const toDay = new Date();

    if (toDay.getFullYear() < checkYear) {
      return true;
    } else if (toDay.getFullYear() === checkYear) {
      if (toDay.getMonth() + 1 < checkMonth) {
        return true;
      } else if (
        toDay.getMonth() + 1 === checkMonth &&
        toDay.getDate() <= checkDay
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }

  }

  monthMinus() {
    const countMonth = +this.month.innerText - 1;
    if (
      this.checkFullDate(+this.day.innerText, countMonth, +this.year.innerText)
    ) {
      if (countMonth > 0 && countMonth < 10) {
        this.month.innerText = `0${countMonth}`;
      } else if (countMonth >= 10 && countMonth < 13) {
        this.month.innerText = `${countMonth}`;
      }
      this.insertDate();
    }
  }

  monthPlus() {
    const countMonth = +this.month.innerText + 1;
    if (
      this.checkFullDate(+this.day.innerText, countMonth, +this.year.innerText)
    ) {
      if (countMonth > 0 && countMonth < 10) {
        this.month.innerText = `0${countMonth}`;
      } else if (countMonth >= 10 && countMonth < 13) {
        this.month.innerText = `${countMonth}`;
      }

      this.insertDate();
    }
  }

  checkDay(countDay, month) {
    if (this.checkFullDate(countDay, month, +this.year.innerText)) {
      if (countDay > 0 && countDay < 10) {
        this.day.innerText = `0${countDay}`;
      } else if (
        month === 1 ||
        month === 3 ||
        month === 5 ||
        month === 7 ||
        month === 8 ||
        month === 10 ||
        month === 12
      ) {
        if (countDay < 32 && countDay > 0) {
          this.day.innerText = `${countDay}`;
        }
      } else if (month !== 2 && countDay < 31 && countDay > 0) {
        this.day.innerText = `${countDay}`;
      } else if (month === 2 && +this.year.innerText % 4 !== 0) {
        if (countDay < 29 && countDay > 0) {
          this.day.innerText = `${countDay}`;
        }
      } else if (countDay < 30 && countDay > 0) {
        this.day.innerText = `${countDay}`;
      }

      this.insertDate();
    }
  }

  dayMinus() {
    const countDay = +this.day.innerText - 1;
    const month = +this.month.innerText;

    this.checkDay(countDay, month);
  }

  dayPlus() {
    const countDay = +this.day.innerText + 1;
    const month = +this.month.innerText;

    this.checkDay(countDay, month);
  }

  yearMinus() {
    const yearCount = +this.year.innerText - 1;
    const toDay = new Date();
    if (
      this.checkFullDate(+this.day.innerText, +this.month.innerText, yearCount)
    ) {
      if (
        yearCount > toDay.getFullYear() ||
        yearCount === toDay.getFullYear()
      ) {
        this.year.innerText = yearCount;
      }
      this.insertDate();
    }
  }

  yearPlus() {
    const yearCount = +this.year.innerText + 1;
    const toDay = new Date();
    if (
      this.checkFullDate(+this.day.innerText, +this.month.innerText, yearCount)
    ) {
      if (yearCount < toDay.getFullYear() + 10) {
        this.year.innerText = yearCount;
      }
      this.insertDate();
    }
  }

  insertDate() {
    this.calendarDate.innerText = `${this.arrMont[+this.month.innerText]}  ${
      this.day.innerText
    },  ${this.year.innerText}`;
  }

  cancel(event) {
    this.mainElemCalendar.classList.add("hidden");
  }

  save(event) {
    this.calendarDateForm.innerText = this.calendarDate.innerText;
    this.mainElemCalendar.classList.add("hidden");
  }

  setDate(data) {
    this.calendarDateForm.innerText = `${
      this.arrMont[data.getMonth() + 1]
    } ${data.getDate()}, ${data.getFullYear()}`;

  }
}
