require("./modal-task.less");
import { Timestamp } from "firebase/firestore/lite";
import { Calendar } from "./calendar";
import { db } from "../../app";
import modalTask from "./modal-task.hbs";

export class ModalTask {
  constructor(titleModal = "Add Task") {
    this.main = document.getElementsByTagName("main")[0];

    this.mainModalTask = document.createElement("div");
    this.mainModalTask.classList.add("modal", "col", "col-s-10", "center");
    this.mainModalTask.innerHTML = modalTask({ title: titleModal });

    this.main.appendChild(this.mainModalTask);
    this.calendar = new Calendar();

  }

  init(titleModal = "Add task") {
    console.log("init");
    this.priority = 4;
    this.category = "work";
    this.initElem();
  }

  initElem(titleModal) {

    this.deadline = document.getElementById("deadline");
    this.title = document.getElementById("title");
    this.description = document.getElementById("description");
    this.cancelTask = document.getElementById("cancel-task");
    this.saveTask = document.getElementById("save-task");

    // this.calendar.setDate(new Date());
    this.taskAddEventListener();
    this.getRadioButton();
    this.getEstimation();
    this.initCalendar();
  }

  getTitle() {
    return this.title.value;
  }

  getDescription() {
    return this.description.value;
  }

  getRadioButton() {
    this.inputRadioList = document.getElementsByClassName("radio-button");
    for (let i = 0; i < this.inputRadioList.length; i++) {
      this.inputRadioList[i].addEventListener(
        "change",
        this.checkRadio.bind(this)
      );
    }
  }

  checkRadio(event) {
    const elemInput = event.target;
    if (elemInput.classList.contains("work")) {
      this.category = "work";
    } else if (elemInput.classList.contains("education")) {
      this.category = "education";
    } else if (elemInput.classList.contains("hobby")) {
      this.category = "hobby";
    } else if (elemInput.classList.contains("sport")) {
      this.category = "sport";
    } else if (elemInput.classList.contains("other")) {
      this.category = "other";
    } else if (elemInput.classList.contains("urgent")) {
      this.priority = 4;
    } else if (elemInput.classList.contains("hight")) {
      this.priority = 3;
    } else if (elemInput.classList.contains("middle")) {
      this.priority = 2;
    } else if (elemInput.classList.contains("low")) {
      this.priority = 1;
    } else {
      this.priority = 0;
    }
  }

  getEstimation() {
    this.elemEstimation = document.getElementsByClassName("img-tomato");
    for (let i = 0; i < this.elemEstimation.length; i++) {
      this.elemEstimation[i].addEventListener(
        "mouseover",
        this.mouseOverEstimate.bind(this)
      );
      this.elemEstimation[i].addEventListener(
        "mouseout",
        this.mouseOutEstimate.bind(this)
      );
      this.elemEstimation[i].addEventListener(
        "click",
        this.clickEstimate.bind(this)
      );
    }
  }

  mouseOverEstimate(event) {
    const elem = event.target;
    this.unChange = true;

    for (let i = 0; i < this.elemEstimation.length; i++) {
      this.elemEstimation[
        i
      ].style.backgroundImage = `url("/images/fill-tomato.svg")`;

      if (elem === this.elemEstimation[i]) {
        break;
      }
    }
  }

  fillEmptyTomato() {
    for (let i = 0; i < this.elemEstimation.length; i++) {
      this.elemEstimation[
        i
      ].style.backgroundImage = `url("/images/empty-tomato.svg")`;
    }
  }

  mouseOutEstimate(event) {
    if (this.unChange) {
      this.fillEmptyTomato();
    }
  }

  clickEstimate(event) {
    const elem = event.target;
    this.fillEmptyTomato();

    for (let i = 0; i < this.elemEstimation.length; i++) {
      this.elemEstimation[
        i
      ].style.backgroundImage = `url("/images/fill-tomato.svg")`;

      if (elem === this.elemEstimation[i]) {
        this.estimation = i + 1;
        break;
      }
    }
    this.unChange = false;
  }

  initCalendar() {
    this.deadline.childNodes[4].addEventListener(
      "click",
      this.getCalendar.bind(this)
    );
  }

  getCalendar(event) {
    this.calendar.mainElemCalendar.classList.remove("hidden");
  }

  taskAddEventListener() {
    this.saveTask.addEventListener("click", this.save.bind(this));
    this.cancelTask.addEventListener("click", this.cancel.bind(this));
  }

  save(event) {
    this.btnSave = document.getElementById("btn-modal-save");
    const id = Math.floor(Date.now() / 1000);

    const objTask = {
      id: `${id}`,
      title: this.title.value,
      description: this.description.value,
      createDate: Timestamp.fromDate(new Date()),
      startDate: Timestamp.fromDate(
        new Date(this.calendar.calendarDateForm.innerText)
      ),
      deadline: Timestamp.fromDate(
        new Date(this.calendar.calendarDateForm.innerText)
      ),
      isActive: false,
      isDone: true,
      estimationTotal: this.estimation,
      estimationUsed: this.estimation,
      priority: this.priority,
      categoryId: this.category,
      isInProgress: false,
      failedPomodoros: 0,
    };

    if (
      document.getElementsByClassName("form-modal-legend")[0].innerText ===
      "Edit Task"
    ) {
      const id = this.mainModalTask.getAttribute("data-set");
      objTask.id = id;
      
      db.updateTask(objTask)
        .then((responce) => {
          console.log("Task completed updated " + objTask);
          this.btnSave.setAttribute("data-route", "task-list");
          this.btnSave.click();
        })
        .catch((error) => {
          console.log("Error Task Didn't Save");
        });

      this.updateLocalStorage(objTask);

    } else {
      db.setTask(objTask)
        .then((responce) => {
          console.log("Task completed saved " + objTask);
          this.btnSave.setAttribute("data-route", "task-list");
          this.btnSave.click();
        })
        .catch((error) => {
          console.log(error);
        });

      const storage = JSON.parse(localStorage.getItem("tasks"));
      storage[storage.length] = objTask;
      localStorage.setItem("tasks", JSON.stringify(storage));
    }

    // const storage=JSON.parse(localStorage.getItem("tasks"));
    // if (document.getElementsByClassName("form-modal-legend")[0].innerText === "Edit Task") {
    //   const id = this.mainModalTask.getAttribute("data-set");
    //   objTask.id = id;

    //   storage.push(objTask);
    //   localStorage.setItem("tasks", JSON.stringify(storage));
    // db.updateTask(objTask);
    // } else {

    //   storage.push(objTask);
    //   localStorage.setItem("tasks", JSON.stringify(storage));

    //   // db.setTask(objTask);

    // }

    // this.main.removeChild(this.mainModalTask);
    // console.log("task completed saved "+objTask);
  }

  updateLocalStorage(task) {
    const storage = JSON.parse(localStorage.getItem("tasks"));
    for (let i = 0; i < storage.length; i++) {
      if (+storage[i].id === +task.id) {
        storage[i] = task;
        break;
      }
    }

    localStorage.setItem("tasks", JSON.stringify(storage));
  }

  cancel() {
    console.log("task is canseled");
  }

  initEstimation(estimation) {
    for (let i = 0; i < estimation; i++) {
      this.elemEstimation[
        i
      ].style.backgroundImage = `url("/images/fill-tomato.svg")`;
    }
    this.estimation = estimation;
  }

  fillModalForEdit(task) {
    this.initElem();

    this.mainModalTask.setAttribute("data-set", `${task.id}`);
    const radioInputCategory = document.querySelector(
      `.radio-${task.categoryId}`
    );
    this.category=task.categoryId;
    if (task.categoryId !== "work") {
      radioInputCategory.setAttribute("checked", "checked");
    }

    this.priority=task.priority;

    const radioInputPriority = document.querySelector(`.radio${task.priority}`);

    if (radioInputPriority && task.priority !== 4) {
      radioInputPriority.setAttribute("checked", "checked");
    }

    

    const date = new Date(task.startDate.seconds * 1000);
    this.title.value = `${task.title}`;
    this.description.value = `${task.description}`;
    this.calendar.setDate(date);

    this.initEstimation(task.estimationUsed - 1);
  }
}
