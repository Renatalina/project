import hendlebars from "./first-entry.hbs";
import navbarHendlebars from "../navbar/navbar.hbs";

export class FirstEntry {
  constructor() {
    this.main = document.getElementsByTagName("main")[0];
    this.init();
  }

  init() {
    this.main.innerHTML = "";
    this.main.classList.value = "";
    this.main.classList.add("add", "col", "col-s-10", "center");
    this.main.innerHTML = hendlebars();

    this.buttonSkip = document.getElementsByClassName("cancel")[0];
    this.buttonSkip.addEventListener("click", this.skip.bind(this));
    this.buttonSkip.setAttribute("data-route", "task-list");

    this.buttonGoToSettings = document.getElementsByClassName("save")[0];
    this.buttonGoToSettings.addEventListener(
      "click",
      this.goToSettings.bind(this)
    );
    this.buttonGoToSettings.setAttribute("data-route", "settings");
  }

  skip() {
    localStorage.setItem("user", "userEntry");
  }

  goToSettings() {
    localStorage.setItem("user", "userEntry");
  }
}
