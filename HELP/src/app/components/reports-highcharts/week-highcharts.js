const Highcharts = require("highcharts");
require("highcharts/modules/exporting")(Highcharts);

export class WeekHighCharts {
  constructor(tasks) {
    this.init(tasks);
  }
  init(tasks) {
    this.createListWeekDiagram(tasks);
  }
  filterListByPriorityDiagram(listTasks) {
    const lists = {
      urgent: [],
      height: [],
      middle: [],
      low: [],
      failed: [],
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent.push(listTasks[i]);
          break;
        case 3:
          lists.height.push(listTasks[i]);
          break;
        case 2:
          lists.middle.push(listTasks[i]);
          break;
        case 1:
          lists.low.push(listTasks[i]);
          break;
        case 0:
          lists.failed.push(listTasks[i]);
          break;
        default:
          break;
      }
    }

    return lists;
  }

  filterListByPriorityPomodoroDiagram(listTasks) {
    const lists = {
      urgent: 0,
      height: 0,
      middle: 0,
      low: 0,
      failed: 0,
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent +=
            listTasks[i].estimationUsed;
          break;
        case 3:
          lists.height +=
            listTasks[i].estimationUsed;
          break;
        case 2:
          lists.middle +=
            listTasks[i].estimationUsed;
          break;
        case 1:
          lists.low +=
            listTasks[i].estimationUsed;
          break;
        case 0:
          lists.failed +=
            listTasks[i].estimationUsed;
          break;
        default:
          break;
      }
    }
    return lists;
  }

  filterByWeek(tasks) {
    let toDay = new Date().getTime();
    let limitData = "";
    let listWithFilterTask = [];

    for (let i = 0; i < 5; i++) {
      limitData = toDay - 86400000;

      listWithFilterTask[i] = tasks.filter((task) => {
        const date = task.startDate.seconds * 1000;

        if (date <= toDay && date >= limitData) {
          return task;
        }
      });

      toDay = toDay - 86400000;
    }

    return listWithFilterTask;
  }

  createTaskObjForHandlebars(fullFilteredTasks) {
    const obj = [];
    const weekDay = [
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
    ];

    for (let i = 0; i < fullFilteredTasks.length; i++) {
      obj.push(
        {
          x: i,
          y:
            fullFilteredTasks[i].urgent.length +
            fullFilteredTasks[i].height.length +
            fullFilteredTasks[i].middle.length +
            fullFilteredTasks[i].low.length,

          name: "URGENT",
          color: "#C94D47",
        },
        {
          x: i,
          y:
            fullFilteredTasks[i].urgent.length +
            fullFilteredTasks[i].height.length +
            fullFilteredTasks[i].middle.length,

          name: "HEIGHT",
          color: "#FFB200",
        },
        {
          x: i,
          y:
            fullFilteredTasks[i].low.length +
            fullFilteredTasks[i].middle.length,
          name: "MIDDLE",
          color: "#FDDC43",
        },
        {
          x: i,
          y: fullFilteredTasks[i].low.length,
          name: "LOW",
          color: "#00D4D9",
        },

        {
          x: `${i}.25`,
          y: fullFilteredTasks[i].failed.length,
          name: "FAILED",
          color: "#8DA5B8",
        },
        {
          weekDay: `${weekDay[new Date().getDay() - 1 + i]}`,
        }
      );
    }
    return obj;
  }
  createPomodoroObjForHandlebars(fullFilteredTasks) {
    const obj = [];
    const weekDay = [
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SUT",
      "SUN",
    ];

    for (let i = 0; i < fullFilteredTasks.length; i++) {
      obj.push(
        {
          x: i,
          y:
            fullFilteredTasks[i].urgent +
            fullFilteredTasks[i].height +
            fullFilteredTasks[i].middle +
            fullFilteredTasks[i].low,

          name: "URGENT",
          color: "#C94D47",
        },
        {
          x: i,
          y:
            fullFilteredTasks[i].urgent +
            fullFilteredTasks[i].height +
            fullFilteredTasks[i].middle,

          name: "HEIGHT",
          color: "#FFB200",
        },
        {
          x: i,
          y: fullFilteredTasks[i].low + fullFilteredTasks[i].middle,
          name: "MIDDLE",
          color: "#FDDC43",
        },
        {
          x: i,
          y: fullFilteredTasks[i].low,
          name: "LOW",
          color: "#00D4D9",
        },

        {
          x: `${i}.25`,
          y: fullFilteredTasks[i].failed,
          name: "FAILED",
          color: "#8DA5B8",
        },
        {
          weekDay: `${weekDay[new Date().getDay() - 1 + i]}`,
        }
      );
    }
    return obj;
  }

  createListWeekDiagram(tasks) {
    const filterTasks = this.filterByWeek(tasks);

    this.initByRouters(filterTasks);
  }

  initByRouters(filterTasks) {
    let fullFilteredTasks = [];
    let fullFilteredPomodoroTasks = [];

    filterTasks.forEach((list) => {
      fullFilteredTasks.push(this.filterListByPriorityDiagram(list));
      fullFilteredPomodoroTasks.push(
        this.filterListByPriorityPomodoroDiagram(list)
      );
    });

    const arrTasks = this.createTaskObjForHandlebars(fullFilteredTasks);
    const arrPomodoro = this.createPomodoroObjForHandlebars(
      fullFilteredPomodoroTasks
    );

    this.createTasks(arrTasks, arrPomodoro);
  }

  createTasks(listTasks, listPomodoro) {
    const listOfCategories = [];
    Array.from(listTasks).forEach((task) => {
      if (task.weekDay) listOfCategories.push(task.weekDay);
    });

    this.fillDiagramHighCharts(listOfCategories, listTasks, listPomodoro);
  }

  fillDiagramHighCharts(listOfCategories, listTasks, listPomodoro) {
    const chart = Highcharts.chart("diagram-week", {
      chart: {
        type: "column",
        backgroundColor: "#2A3F50",
      },
      title: {
        text: "",
      },
      xAxis: {
        categories: listOfCategories,
        crosshair: true,
      },
      yAxis: {
        min: 0,
        tickInterval: 2,
        title: {
          text: "",
        },
      },
      legend: {
        itemHiddenStyle: {
          color: "#8DA5B8",
        },
        itemStyle: {
          color: "white",
        },
      },
      tooltip: {
        pointFormat: `<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> 
          ({point.percentage:.0f}%)<br/>`,
        shared: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0,
          borderWidth: 0,
          pointWidth: 15,
        },
      },
      series: [
        {
          name: "Pomodoro",
          color: "transparent",
          type: "column",
          data: listPomodoro,
        },
        {
          name: "Tasks",
          color: "transparent",
          data: listTasks,
        },
      ],
    });
  }
}
