const Highcharts = require("highcharts");
require("highcharts/modules/exporting")(Highcharts);

export class DayHighCharts {
  constructor(tasks) {
    this.init(tasks);
  }

  init(tasks) {
    const listTasks = this.filterListByPriorityDiagram(tasks);
    const listPomodoroTasks = this.filterListByPomodoroDiagram(tasks);

    this.fillDiagram(listTasks, listPomodoroTasks);
  }

  filterListByPriorityDiagram(listTasks) {
    const lists = {
      urgent: [],
      heigh: [],
      middle: [],
      low: [],
      failed: [],
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent.push(listTasks[i]);
          break;
        case 3:
          lists.heigh.push(listTasks[i]);
          break;
        case 2:
          lists.middle.push(listTasks[i]);
          break;
        case 1:
          lists.low.push(listTasks[i]);
          break;
        case 0:
          lists.failed.push(listTasks[i]);
          break;
        default:
          break;
      }
    }

    return lists;
  }

  filterListByPomodoroDiagram(listTasks) {
    const lists = {
      urgent: 0,
      heigh: 0,
      middle: 0,
      low: 0,
      failed: 0,
    };

    for (let i = 0; i < listTasks.length; i++) {
      switch (listTasks[i].priority) {
        case 4:
          lists.urgent +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 3:
          lists.heigh +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 2:
          lists.middle +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 1:
          lists.low +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        case 0:
          lists.failed +=
            listTasks[i].estimationTotal - listTasks[i].failedPomodoros;
          break;
        default:
          break;
      }
    }

    return lists;
  }

  fillDiagram(listTasks, listPomoro) {
    const chart = Highcharts.chart("diagram-day", {
      chart: {
        type: "column",
        backgroundColor: "#2A3F50",
      },
      title: {
        text: "",
      },
      xAxis: {
        categories: ["URGENT", "HEIGH", "MIDDLE", "LOW", "FAILED"],
        crosshair: true,
      },
      yAxis: {
        min: 0,
        max: 10,
        tickInterval: 2,
        title: {
          text: "",
        },
      },
      legend: {
        itemHiddenStyle: {
          color: "#8DA5B8",
        },
        itemStyle: {
          color: "white",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:f} {series.name}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,

        },
      },
      series: [
        {
          name: "Pomodoro",
          color: "transparent",
          marker: {
            enabled: false,
          },

          data: [
            { y: listPomoro.urgent, color: "#F15A4A" },
            { y: listPomoro.heigh, color: "#FEA741" },
            { y: listPomoro.middle, color: "#FDDC43" },
            { y: listPomoro.low, color: "#1ABC9C" },
            { y: listPomoro.failed, color: "#8DA5B8" },
          ],
        },
        {
          name: "Task",
          color: "transparent",
          marker: {
            enabled: false,
          },
          data: [
            { y: listTasks.urgent.length, color: "#F15A4A" },
            { y: listTasks.heigh.length, color: "#FEA741" },
            { y: listTasks.middle.length, color: "#FDDC43" },
            { y: listTasks.low.length, color: "#1ABC9C" },
            { y: listTasks.failed.length, color: "#8DA5B8" },
          ],
        },
      ],
    });
  }
}
