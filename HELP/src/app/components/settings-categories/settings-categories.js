require("./settings-categories.less");

export class SettingsCategories {
  constructor() {
    this.main = document.getElementsByClassName("main")[0];
    this.init();
    this.createFooter();
  }

  init() {
    this.main.innerHTML = "";
    this.main.classList.value = "";
    this.main.classList.add("main", "main-settings", "col", "center", "mt-1");
    this.mainElemArticle = this.createElement(
      "article",
      "article-settings",
      "col",
      "col-8",
      "a-start"
    );
    this.work = this.createNode({ paragraph: "Work", color: "selective" });
    this.education = this.createNode({
      paragraph: "Education",
      color: "maliby",
    });
    this.hobby = this.createNode({ paragraph: "Hobby", color: "lilac" });
    this.sport = this.createNode({ paragraph: "Sport", color: "tomato" });
    this.other = this.createNode({ paragraph: "Other", color: "turquoise" });
    this.mainElemArticle.appendChild(this.work);
    this.mainElemArticle.appendChild(this.education);
    this.mainElemArticle.appendChild(this.hobby);
    this.mainElemArticle.appendChild(this.sport);
    this.mainElemArticle.appendChild(this.other);
    this.main.appendChild(this.mainElemArticle);
  }

  createNode(objNode) {
    const divParent = this.createElement("div", "div-timer", "row", "col-s-10");
    const divCircle = this.createElement("div", "circle", objNode.color);
    divParent.appendChild(divCircle);
    const divTimer = this.createElement("div", "div-timer-href");
    const paragraphElem = this.createElement("p");
    paragraphElem.innerText = `${objNode.paragraph}`;
    divTimer.appendChild(paragraphElem);
    divParent.appendChild(divTimer);

    return divParent;
  }

  createFooter() {
    this.footer = document.getElementsByTagName("footer");
     for (let i=0; i< this.footer.length;i++){
       this.footer[i].innerHTML = "";
     }
    this.footer[0].classList.value = "";
    
    this.footer[0].classList.add("footer-settings", "col", "col-s-9", "center");
    this.button = document.createElement("button");
    this.button.classList.add("btn", "btn-go", "col", "col-s-10", "center");
    this.button.innerText = "Go To Task";
    this.button.setAttribute("data-route", "task-list");

    this.footer[0].appendChild(this.button);
  }

  createElement(tagName, ...classHtml) {
    const elem = document.createElement(tagName);
    classHtml.forEach((html) => {
      elem.classList.add(html);
    });

    return elem;
  }
}
