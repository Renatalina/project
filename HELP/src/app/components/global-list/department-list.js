require("./global-list.less");
import { TaskForDaily } from "../daily-task/task";

export class DepartmentList {
  constructor(colorDepartment, colorText, colorPomodoro) {
    this.templateDepartment = document.getElementById("template-department");

    this.department =
      this.templateDepartment.content.cloneNode(true).childNodes[1];
    this.department.classList.add("hidden");

    this.radioButton =
      this.department.childNodes[1].childNodes[1].childNodes[1];
    this.radioButton.addEventListener("click", this.showTaskList.bind(this));
    this.radioButton.classList.add(colorText);

    this.title = this.department.childNodes[1].childNodes[1].childNodes[3];
    this.title.classList.add(colorText);

    this.border = this.department.childNodes[1].childNodes[3];
    this.border.classList.add(colorDepartment);

    this.elemAddTask =
      this.department.childNodes[1].childNodes[3].childNodes[1];
    this.arrTask = [];
  }

  addTask() {
    const task = new TaskForDaily("transparent", "selective", "tomato-t");
    task.task.removeChild(task.shrink);
    task.up.classList.remove("hidden");
    task.day.classList.remove("hidden");
    this.arrTask.push(task);
    this.elemAddTask.appendChild(task.task);
  }

  showTaskList() {
    if (this.department.childNodes[1].childNodes[3].hasAttribute("data-set")) {
      this.department.childNodes[1].childNodes[3].classList.remove("hidden");
      this.department.childNodes[1].childNodes[3].removeAttribute("data-set");
    } else {
      this.department.childNodes[1].childNodes[3].setAttribute(
        "data-set",
        "closed"
      );
      this.department.childNodes[1].childNodes[3].classList.add("hidden");
    }
  }
}
