require("./global-list.less");
import department from "./department-handlebars.hbs";
import globallist from "./globallist-handlebars.hbs";
import tasksHandlebars from "./task-handlebars.hbs";
import taskRemove from "./task-remove.hbs";
import navbarHandlebarsRemove from "../navbar/navbar-remove.hbs";
import { db } from "../../app";
import { Timestamp } from "firebase/firestore/lite";
import { taskChangedEventBas } from "../../pages/tasks-list";
import { ModalTask } from "../modal-task/modal-task";
import { header } from "../../app";
import { Timer } from "../../pages/timer";

export class GlobalList {
  constructor(param) {
    this.init(param);
  }

  init(param) {
    this.footer = document.getElementsByTagName("footer")[0];
    this.parameterOfTasks=param;
    this.listWork = [];
    this.listEducation = [];
    this.listSport = [];
    this.listOther = [];
    this.listHobby = [];
    this.fullListTasks = [];

    this.sectionElementNav = document.createElement("section");
    this.sectionElementNav.classList.add(
      "footer-daily",
      "col",
      "col-s-10",
      "col-md-9",
      "center"
    );
    this.sectionElementNav.innerHTML = globallist();

    this.sectionElementDepartment = document.createElement("section");
    this.sectionElementDepartment.classList.add(
      "section",
      "section-global-list",
      "col",
      "col-10",
      "center"
    );

    this.footer.appendChild(this.sectionElementNav);

    this.initGlobalNav();
  }

  initGlobalNav() {
    this.openGlobalList = document.getElementsByClassName(
      "icon-global-list-arrow-right"
    )[0];
    this.openGlobalList.addEventListener("click", this.openList.bind(this));

    this.globallistNavSort =
      document.getElementsByClassName("footer-daily-nav")[0];
    
    this.selectedAllRemove=document.getElementsByClassName("global-remove-select")[0]; 

    this.initSortNav();
  }

  initSortNav() {
    this.sortAll = document.getElementById("all-tasks");
    this.sortAll.addEventListener("click", this.filterByAll.bind(this));
    this.sortUrgent = document.getElementById("urgent-tasks");
    this.sortUrgent.addEventListener("click", this.filterByUrgent.bind(this));
    this.sortHight = document.getElementById("hight-tasks");
    this.sortHight.addEventListener("click", this.filterByHight.bind(this));
    this.sortMiddle = document.getElementById("middle-tasks");
    this.sortMiddle.addEventListener("click", this.filterByMiddle.bind(this));
    this.sortLow = document.getElementById("low-tasks");
    this.sortLow.addEventListener("click", this.filterByLow.bind(this));
    this.select=document.getElementById("global-select");
    this.select.addEventListener("click", this.selectAll.bind(this));
    this.deselect=document.getElementById("global-deselect");
    this.deselect.addEventListener("click", this.deselectAll.bind(this));
  }

  getTaskFromDB() {
    // db.getTasks().then((response) => {
    //   this.fillGlobalListByDepartment(response);
    // });

    const storage = JSON.parse(localStorage.getItem("tasks"));
    this.fillGlobalListByDepartment(storage);
    this.fillFullList(storage);
  }

  fillFullList(taskList) {
    this.fullListTasks = [];
    taskList.forEach((task) => {
      const date = new Date(task.startDate.seconds * 1000);

      if (!this.checkTaskNotForToDay(date)) {
        this.fullListTasks.push(task);
      }
    });
  }

  fillGlobalListByDepartment(taskList) {
    this.listWork = [];
    this.listEducation = [];
    this.listSport = [];
    this.listOther = [];
    this.listHobby = [];

    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    taskList.forEach((task) => {
      const date = new Date(task.startDate.seconds * 1000);

      if (!this.checkTaskNotForToDay(date)) {
        task.day = date.getDate();
        task.month = monthNames[date.getMonth()];

        switch (task.categoryId) {
          case "work":
            this.listWork.push(task);
            break;
          case "sport":
            this.listSport.push(task);
            break;
          case "other":
            this.listOther.push(task);
            break;
          case "education":
            this.listEducation.push(task);
            break;
          case "hobby":
            this.listHobby.push(task);
            break;
          default:
            break;
        }
      }
    });

    this.addTask();
  }

  checkTaskNotForToDay(date) {
    const toDay = new Date();
    return toDay.getDay() === date.getDay();
  }

  addTask() {
    this.sectionElementDepartment.innerHTML = "";
    if (this.listWork.length > 0) {
      this.initMainElemSectionGlobalList("WORK", this.listWork);
    }
    if (this.listEducation.length > 0) {
      this.initMainElemSectionGlobalList("EDUCATION", this.listEducation);
    }
    if (this.listHobby.length > 0) {
      this.initMainElemSectionGlobalList("HOBBY", this.listHobby);
    }
    if (this.listOther.length > 0) {
      this.initMainElemSectionGlobalList("OTHER", this.listOther);
    }
    if (this.listSport.length > 0) {
      this.initMainElemSectionGlobalList("SPORT", this.listSport);
    }
    this.addEventHideDepartment();
    this.sectionElementNav.appendChild(this.sectionElementDepartment);
    this.addEventListenerToTask();
    this.addEventListenerToRemoveTask();
  }

  initMainElemSectionGlobalList(categoryId, listTasks) {
    const divElemMainSection = document.createElement("div");
    divElemMainSection.classList.add(
      "section-part",
      "col",
      "col-10",
      "center",
      "mb-0",
      "mt-1"
    );

    const includeDepartmentElem = document.createElement("div");
    includeDepartmentElem.classList.add(
      "section-part-header",
      "col",
      "col-10",
      "j-start"
    );
    includeDepartmentElem.innerHTML = department({ categoryId });

    includeDepartmentElem.appendChild(this.newTasks(listTasks, categoryId));

    divElemMainSection.appendChild(includeDepartmentElem);
    this.sectionElementDepartment.appendChild(divElemMainSection);
  }

  addEventHideDepartment() {
    setTimeout(() => {
      const listDepartment = document.getElementsByClassName(
        "department-radio-button"
      );

      for (let i = 0; i < listDepartment.length; i++) {
        listDepartment[i].addEventListener("click", this.hideElems.bind(this));
      }
    }, 1000);
  }

  hideElems(event) {
    const target = event.target;
    const node = target.parentNode.nextSibling.nextSibling;
    if (node.hasAttribute("data-set")) {
      node.removeAttribute("data-set");
      node.classList.add("hidden");
    } else {
      node.setAttribute("data-set", "open");
      node.classList.remove("hidden");
    }
  }

  newTasks(listTasks, categoryId) {
    const divListTasks = document.createElement("div");
    divListTasks.classList.add(
      "section-border-consist",
      "mt-0",
      "col-10",
      categoryId
    );
    divListTasks.setAttribute("data-set", "open");

    const divIncludingTaskList = document.createElement("div");
    divIncludingTaskList.classList.add(
      "section-part-task",
      "col",
      "col-10",
      "a-start",
      "mt-14"
    );
    if(this.parameterOfTasks){
      divIncludingTaskList.innerHTML = taskRemove({ task: listTasks });     
    }else{
       divIncludingTaskList.innerHTML = tasksHandlebars({ task: listTasks });
    }   

    divListTasks.appendChild(divIncludingTaskList);

    return divListTasks;
  }

  openList() {
    if (this.sectionElementNav.hasAttribute("data-set")) {
      this.sectionElementNav.removeAttribute("data-set");
      this.globallistNavSort.classList.add("hidden");
      if(this.parameterOfTasks){
        this.selectedAllRemove.classList.add("hidden");
      }    

      this.openGlobalList.classList.remove("icon-global-list-arrow-down");
      this.openGlobalList.classList.add("icon-global-list-arrow-right");

      this.sectionElementDepartment.classList.add("hidden");
    } else {
      this.sectionElementNav.setAttribute("data-set", "open");
      this.globallistNavSort.classList.remove("hidden");
      if(this.parameterOfTasks){
        this.selectedAllRemove.classList.remove("hidden");
      }    

      this.openGlobalList.classList.add("icon-global-list-arrow-down");
      this.openGlobalList.classList.remove("icon-global-list-arrow-right");

      this.sectionElementDepartment.classList.remove("hidden");

      this.getTaskFromDB();
    }
  }

  addEventListenerToTask() {
      const listTasksUpToToday =
        document.getElementsByClassName("global-arrows");
      const listTasksEdit = document.getElementsByClassName("global-edit");
      const listTasksRemove = document.getElementsByClassName("global-trash");
      const listGoToTimer = document.getElementsByClassName("change-pomodoro");

      for (let i = 0; i < listTasksUpToToday.length; i++) {
        listTasksUpToToday[i].addEventListener(
          "click",
          this.taskToUp.bind(this)
        );
        listTasksEdit[i].addEventListener("click", this.taskEdit.bind(this));
        listTasksRemove[i].addEventListener("click",this.taskRemove.bind(this));
        listGoToTimer[i].addEventListener("click", this.goToTimer.bind(this));
      }   
  }

  addEventListenerToRemoveTask(){
    const listOfRemoveTask=document.getElementsByClassName("remove-global");
    for (let i=0; i<listOfRemoveTask.length;i++){
      listOfRemoveTask[i].addEventListener("click", this.removeListTask.bind(this));
    }
  }

  taskToUp(event) {
    const idElem = event.target.getAttribute("data-set");
    this.fullListTasks.filter((task) => {
      if (+task.id === +idElem) {
        task.startDate = Timestamp.fromDate(new Date());
        delete task.day;
        delete task.month;
        db.updateTask(task);
        this.updateLocalStorage(task);

        this.getTaskFromDB();
        this.notifyChanges(task);
        return true;
      }
    });
  }

  updateLocalStorage(task) {
    const storage = JSON.parse(localStorage.getItem("tasks"));
    for (let i = 0; i < storage.length; i++) {
      if (+storage[i].id === +task.id) {
        storage[i] = task;
        break;
      }
    }

    localStorage.setItem("tasks", JSON.stringify(storage));
  }

  taskEdit(event) {

    const idElem = event.target.getAttribute("data-set");
    this.fullListTasks.filter((task) => {
      if (+task.id === +idElem) {
        delete task.day;
        delete task.month;
        const modalEditTask = new ModalTask("Edit Task");
        modalEditTask.fillModalForEdit(task);
        this.getTaskFromDB();
        this.notifyChanges(task);
        return true;
      }
    });
  }

  taskRemove(event) {
    const idElem = event.target.getAttribute("data-set");
    this.fullListTasks.filter((task) => {
      if (+task.id === +idElem) {
        db.deleteTask(task);
        this.deleteTaskFromLocalStorage(task);
        this.getTaskFromDB();
        return true;
      }
    });
  }

  deleteTaskFromLocalStorage(task) {
    const storage = JSON.parse(localStorage.getItem("tasks"));

    const updateStorage = storage.filter((elem) => {
      if (+elem.id !== +task.id) {
        return elem;
      }
    });

    localStorage.setItem("tasks", JSON.stringify(updateStorage));
  }

  goToTimer(event) {
    console.log("go to timer");    
  }

  filterByAll() {
    this.fillGlobalListByDepartment(this.fullListTasks);
  }

  filterByUrgent() {
    const taskList = this.fullListTasks.filter((task) => {
      if (+task.priority === 4) {
        return task;
      }
    });
    this.fillGlobalListByDepartment(taskList);
  }

  filterByHight() {
    const taskList = this.fullListTasks.filter((task) => {
      if (+task.priority === 3) {
        return task;
      }
    });
    this.fillGlobalListByDepartment(taskList);
  }

  filterByMiddle() {
    const taskList = this.fullListTasks.filter((task) => {
      if (+task.priority === 2) {
        return task;
      }
    });
    this.fillGlobalListByDepartment(taskList);
  }

  filterByLow() {
    const taskList = this.fullListTasks.filter((task) => {
      if (+task.priority === 1) {
        return task;
      }
    });
    this.fillGlobalListByDepartment(taskList);
  }




  checkFindTask(taskId) {
    const storage = JSON.parse(localStorage.getItem("global-remove"));
    for (let i = 0; i < storage.length; i++) {
      if (+storage[i] === +taskId) {
        if(i===0){
          return "0";
        }else{
          return i;
        }        
      }
    }
  }

  addRemoveTask(taskId) {    
    if(!this.checkFindTask(taskId)){
      const storage = JSON.parse(localStorage.getItem("global-remove"));
      storage.push(taskId);
      localStorage.setItem("global-remove", JSON.stringify(storage));
    }
  }

  deleteRemoveTask(taskId){
    const storage=JSON.parse(localStorage.getItem("global-remove"));
    delete storage[+this.checkFindTask(taskId)];
    localStorage.setItem("global-remove", JSON.stringify(storage));   
  }

  removeListTask(event) {
    const elem = event.target;  
    if (elem.classList.contains("icon-trash")) {      
      elem.classList.remove("icon-trash");
      elem.classList.add("icon-close");
      this.addRemoveTask(elem.getAttribute("data-set"));      
    }else if(elem.classList.contains("icon-close")){
      elem.classList.add("icon-trash");
      elem.classList.remove("icon-close");
     this.deleteRemoveTask(elem.getAttribute("data-set"));
    }
    header.toCountOfRemove();
  }

  selectAll() {
    const listElemRemove = document.getElementsByClassName("remove-global");
    const arrRemove = [];
    for (let i = 0; i < listElemRemove.length; i++) {
      listElemRemove[i].classList.remove("icon-trash");
      listElemRemove[i].classList.add("icon-close");
      arrRemove.push(listElemRemove[i].getAttribute("data-set"));
    }

    localStorage.setItem("global-remove", JSON.stringify(arrRemove));
    header.toCountOfRemove();
  }

  deselectAll() {
    const listElemRemove = document.getElementsByClassName("remove-global");   
    for (let i = 0; i < listElemRemove.length; i++) {
      listElemRemove[i].classList.add("icon-trash");
      listElemRemove[i].classList.remove("icon-close");      
    }
    localStorage.setItem("global-remove", JSON.stringify([]));
    header.toCountOfRemove();
    
  }

  notifyChanges(newValue) {
    taskChangedEventBas.notify(newValue);
  }
}
